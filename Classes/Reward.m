classdef Reward < handle
    properties
        x = 0;
        y = 0;
        vx = 0;
        vy = 0;
        bounce = true
        shape = [];
        shape_size = 6;
        layer = 2
    end
    methods
        function obj = Reward()
            shape = strel('disk',obj.shape_size);
            obj.shape = shape.Neighborhood;
        end
    end
end