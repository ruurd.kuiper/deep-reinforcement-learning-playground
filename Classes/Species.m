classdef Species
    properties
        animal
        experience
        rewards
        averageReward

        actor
        critic
        averageGradActor
        averageSqGradActor
        averageGradCritic
        averageSqGradCritic
        
        lineAverageReward
        lineLossActor
        lineReward
        
        sequenceInputSize = 12;
        layerSize = 64;
    end
    methods
        function obj = Species(nr)
            % Create a number of animals for this species
            obj.animal = Animal;
            for i = 1:nr
                obj.animal(i) = Animal;
            end
            
            % Create actor and critic networks
            obj.actor = createActorNetwork(obj.sequenceInputSize,obj.layerSize);
            obj.critic = createCriticNetwork(obj.sequenceInputSize,obj.layerSize);
            
            % Set adam gradients to zero
            obj.averageGradActor = [];
            obj.averageSqGradActor = [];
            obj.averageGradCritic = [];
            obj.averageSqGradCritic = [];
            
            obj.lineAverageReward = animatedline('Color','red');
            obj.lineLossActor = animatedline('Color','blue');
            obj.lineReward = animatedline('Color','green');
        end
    end
end