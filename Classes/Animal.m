classdef Animal < handle
    properties
        x = 0;
        y = 0;
        angle = rand*360;
        speed = 0;
        vx = 0;
        vy = 0;
        shape = [];
        bounce = true
        layer = 3;
        experience = {};
        shape_size = 6;
    end
    methods
        function obj = Animal()
            shape = strel('octagon',obj.shape_size);
            obj.shape = shape.Neighborhood;
        end
    end
end