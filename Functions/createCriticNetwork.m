function critic = createCriticNetwork(sequenceInputSize,layerSize)
layers = [
    sequenceInputLayer(sequenceInputSize, 'Name', 'input', 'Normalization', 'none')
    lstmLayer(layerSize, 'Name', 'lstm', 'OutputMode', 'last')
    leakyReluLayer('Name', 'relu1')
    fullyConnectedLayer(1, 'Name', 'fcFinal')];
lgraphCritic = layerGraph(layers);
critic = dlnetwork(lgraphCritic);
end