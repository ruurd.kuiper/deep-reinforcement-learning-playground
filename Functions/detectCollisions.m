function detectCollisions(gameObjects)
%% This function checks an array of objects for collision
% Start checking for collisions
for i = 1:length(gameObjects)
    obj1 = gameObjects{i};
    for j = i+1:length(gameObjects)
        obj2 = gameObjects{j};
        if (pixelIntersect(obj1.x, obj1.y, obj1.shape, obj2.x, obj2.y, obj2.shape))
            if obj1.bounce == true || obj2.bounce == true
                calculateBounce(obj1,obj2);
            end
        end
    end
end
