function [sight,sightLine] = lineOfSightAngular(matrix,location,rays,distance,shape)
% Create the lines
steps = 20;

%% First calculate all the unit vectors
a = 2*pi/rays:2*pi/rays:2*pi;
unit_vector = [cos(a') sin(a')];
vector = unit_vector*distance;
for i = 1:rays
    sightLine{i}(:,1) = round(linspace(unit_vector(i,1)*(size(shape,1)/2+10), vector(i,1), steps))+location(1);
    sightLine{i}(:,2) = round(linspace(unit_vector(i,2)*(size(shape,2)/2+10), vector(i,2), steps))+location(2);
end

if isa(matrix, 'gpuArray')
    v = gpuArray(zeros([rays,3]));
    I = gpuArray(ones(rays,1)*steps);
else
    v = zeros([rays,3]);
    I = ones(rays,1)*steps;
end
for i = 1:length(sightLine)
    % Next, remove all sightline rows that include pixels outside the border
    removeRows = sightLine{i}(:,1) < 1 | sightLine{i}(:,2) < 1 | ...
        sightLine{i}(:,1) > size(matrix,1) | sightLine{i}(:,2) > size(matrix,2);
    sightLine{i}(removeRows,:) = [];
    % Then, write the categorical number of each line to 'v', and it's
    % location along the sightLine to 'I'
    for j = 1:size(sightLine{i},1)
        if any(matrix(sightLine{i}(j,1),sightLine{i}(j,2),:) ~= 0)
            I(i) = j;
            v(i,:) = squeeze(matrix(sightLine{i}(j,1),sightLine{i}(j,2),1:3));
            break
        end
    end
end

I = I/steps;
v(v>0) = 1;
sight = [I(:);v(:)];