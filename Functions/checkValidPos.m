function valid = checkValidPos(map,obj,gameObjects)
%% This function checks if a position is free
% Start checking for collisions
for i = 1:length(gameObjects)
%% Check for outside of bounds
        if obj.x+ceil(size(obj.shape,1)/2) > size(map,1) || obj.x-ceil(size(obj.shape,1)/2) < 1 ...
                || obj.y+ceil(size(obj.shape,2)/2) > size(map,2) || obj.y-ceil(size(obj.shape,2)/2) < 1 
                valid = false;
            return;
        end
%     if obj ~= gameObjects{i}
%         %% Check for overlap with other object
%         if (pixelIntersect(obj.x, obj.y, obj.shape, gameObjects{i}.x, gameObjects{i}.y, gameObjects{i}.shape))
%             valid = false;
%             return
%         end
%     end
end
valid = true;