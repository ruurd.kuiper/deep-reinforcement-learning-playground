function detectEdgeCollisions(gameObjects,map,step)
%% This function checks an array of objects for edge collision

restitution = 0.9;
for i = 1:length(gameObjects)
    obj = gameObjects{i};
    
    % Check for left and right
    if (obj.x-ceil(size(obj.shape,1)/2)) < 1
        obj.vx = abs(obj.vx) * restitution;
        obj.x = ceil(size(obj.shape,1)/2);
        if i == 3
            gameObjects{1}.experience{step,3} = gameObjects{1}.experience{step,3} + 1;
            gameObjects{2}.experience{step,3} = gameObjects{2}.experience{step,3} - 1;
            gameObjects{1}.x = round(size(map,1) * 1/8);
            gameObjects{1}.vx = 0;
            gameObjects{1}.vy = 0;
            gameObjects{2}.x = round(size(map,1) * 7/8);
            gameObjects{2}.vx = 0;
            gameObjects{2}.vy = 0;
            obj.x = round(size(map,1)/2);
            obj.y = round(size(map,2)/2);
            obj.vx = 0;
            obj.vy = 0;
        end
%         obj.experience{step,3} = obj.experience{step,3} - 3;
%         for j = 1:length(gameObjects)
%             gameObjects{j}.experience{step,3} = gameObjects{j}.experience{step,3} + 2;
%         end
    elseif (obj.x+ceil(size(obj.shape,1)/2)) > size(map,1)
        obj.vx = -abs(obj.vx) * restitution;
        obj.x = size(map,1) - ceil(size(obj.shape,1)/2);
        if i == 3
            gameObjects{1}.experience{step,3} = gameObjects{1}.experience{step,3} - 1;
            gameObjects{2}.experience{step,3} = gameObjects{2}.experience{step,3} + 1;
            gameObjects{1}.x = round(size(map,1) * 1/8);
            gameObjects{1}.vx = 0;
            gameObjects{1}.vy = 0; 
            gameObjects{2}.x = round(size(map,1) * 7/8);
            gameObjects{2}.vx = 0;
            gameObjects{2}.vy = 0;
            obj.x = round(size(map,1)/2);
            obj.y = round(size(map,2)/2);
            obj.vx = 0;
            obj.vy = 0;
        end
%         obj.experience{step,3} = obj.experience{step,3} - 3;
%         for j = 1:length(gameObjects)
%             gameObjects{j}.experience{step,3} = gameObjects{j}.experience{step,3} + 2;
%         end
    end

    % Check for bottom and top
    if  obj.y-ceil(size(obj.shape,2)/2) < 1
        obj.vy = abs(obj.vy) * restitution;
        obj.y = ceil(size(obj.shape,2)/2);
%         obj.experience{step,3} = obj.experience{step,3} - 3;
%         for j = 1:length(gameObjects)
%             gameObjects{j}.experience{step,3} = gameObjects{j}.experience{step,3} + 2;
%         end
    elseif obj.y+ceil(size(obj.shape,2)/2) > size(map,2)
        obj.vy = -abs(obj.vy) * restitution;
        obj.y = size(map,2) - ceil(size(obj.shape,2)/2);
%         obj.experience{step,3} = obj.experience{step,3} - 3;
%         for j = 1:length(gameObjects)
%             gameObjects{j}.experience{step,3} = gameObjects{j}.experience{step,3} + 2;
%         end
    end
end