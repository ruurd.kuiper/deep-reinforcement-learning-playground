function map = drawObjectInMap(map,obj)
%% This functions draws an object in the map at a certain location and layer
xrange = obj.x-floor(size(obj.shape,1)/2):obj.x+ceil(size(obj.shape,1)/2)-1;
yrange = obj.y-floor(size(obj.shape,2)/2):obj.y+ceil(size(obj.shape,2)/2)-1;

% for i = 1:size(obj.shape,1)
%     for j = 1:size(obj.shape,2)
%         if obj.shape(i,j) ~= 0
%             map(xrange(i),yrange(j),obj.layer) = obj.shape(i,j);
%         end
%     end
% end

map(xrange,yrange,obj.layer) = obj.shape;