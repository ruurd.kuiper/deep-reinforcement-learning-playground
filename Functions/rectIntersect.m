function overlap = rectIntersect(x1, y1, w1, h1, x2, y2, w2, h2)
%% Check if two rectangles intersect
if (x2 > w1 + x1 || x1 > w2 + x2 || y2 > h1 + y1 || y1 > h2 + y2)
    overlap = true;
end
