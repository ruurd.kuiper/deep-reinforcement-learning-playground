function actor = createActorNetwork(sequenceInputSize,layerSize)
layers = [
    sequenceInputLayer(sequenceInputSize, 'Name', 'input', 'Normalization', 'none')
    lstmLayer(layerSize, 'Name', 'lstm', 'OutputMode', 'last')
    leakyReluLayer('Name', 'relu1')
    fullyConnectedLayer(4, 'Name', 'fcFinal')];
lgraphActor = layerGraph(layers);
actor = dlnetwork(lgraphActor);
end