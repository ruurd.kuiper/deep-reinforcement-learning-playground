function calculateBounce(obj1,obj2)
%% Calculate the change in velocity due to a bounce between two objects

% Calculate collision vector
vCollision(1) = obj2.x - obj1.x;
vCollision(2) = obj2.y - obj1.y;

% Calculate distance between the objects
distance = sqrt((obj2.x-obj1.x)*(obj2.x-obj1.x) + (obj2.y-obj1.y)*(obj2.y-obj1.y));

% Calculate normalized collision vector
vCollisionNorm = vCollision/distance;

% Calculate relative velocity
vRelativeVelocity(1) = obj1.vx - obj2.vx;
vRelativeVelocity(2) = obj1.vy - obj2.vy;

% Calculate speed of collision
speed = vRelativeVelocity(1) * vCollisionNorm(1) + vRelativeVelocity(2) * vCollisionNorm(2);

if speed < 0
    return
end

obj1.vx = obj1.vx - (speed * vCollisionNorm(1));
obj1.vy = obj1.vy - (speed * vCollisionNorm(2));
obj2.vx = obj2.vx + (speed * vCollisionNorm(1));
obj2.vy = obj2.vy + (speed * vCollisionNorm(2));