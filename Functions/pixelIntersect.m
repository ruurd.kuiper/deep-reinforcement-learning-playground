function overlap = pixelIntersect(x1, y1, shape1, x2, y2, shape2)
%% Check if two 2D shapes intersect
% x1, y1, x2, y2 = location on map in 2D for both objects
% shape1, shape2 = shape of both objects as a 2D binary matrix
[s1(:,1),s1(:,2)] = find(shape1);
s1(:,1) = s1(:,1)+x1-floor(size(shape1,1)/2);
s1(:,2) = s1(:,2)+y1-floor(size(shape1,2)/2);

[s2(:,1),s2(:,2)] = find(shape2);
s2(:,1) = s2(:,1)+x2-floor(size(shape2,1)/2);
s2(:,2) = s2(:,2)+y2-floor(size(shape2,2)/2);

if intersect(s1,s2,'rows')
    overlap = true;
else
    overlap = false;
end
