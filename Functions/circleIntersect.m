function overlap = circleIntersect(x1, y1, r1, x2, y2, r2)
%% Check if two circles intersect
% x1,y1,x2,y2 = location of map of both circles
% r1,r2 = radii of both circles

% Calculate the distance between the two circles
squareDistance = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2);

% When the distance is smaller or equal to the sum of the two radii,
% the circles touch or overlap
overlap = squareDistance <= ((r1 + r2) * (r1 + r2));