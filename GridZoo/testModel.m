%% Test Model
X = gridWorld;
    
% Set the start locations of the agent and the reward
% agentLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
% rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
agentLoc = [1,1];
rewardLoc = [5,5];%; 3,3; 5,3; 1,4];
% while all(agentLoc == rewardLoc)
%     rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
% end

% Color the matrix
X(agentLoc(1),agentLoc(2)) = 0;
for i = 1:size(rewardLoc,1)
    X(rewardLoc(i,1),rewardLoc(i,2)) = 3;
end
% Convert mini-batch of data to dlarray.
dlX = dlarray(single(X),'SSCB');

% If training on a GPU, then convert data to gpuArray.
if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
    dlX = gpuArray(dlX);
end

% experience is a cell array filled with:
% State, Action, Reward and State+1
experience = cell(20,4);
figure; hold on;
for step = 1:maxSteps
    imagesc(extractdata(dlX))
    set(gca,'YDir','normal')
    drawnow
    pause(0.1)
    action = forward(actor,dlX);
    [~,actionNr] = max(extractdata(action));
    actionNr = randsample(numel(extractdata(action)), 1, true, extractdata(sigmoid(action)));
    
    dlX(agentLoc(1),agentLoc(2)) = 1;
    
    newLoc = agentLoc + walking(actionNr,:);
    
    if all(newLoc > 0) && all(newLoc <= size(gridWorld))
        agentLoc = newLoc;
    end
    
    if dlX(agentLoc(1),agentLoc(2)) == 3
        reward = 10;
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            while all(agentLoc == rewardLoc)
                rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            end
            dlX(rewardLoc(1),rewardLoc(2)) = 3;
    else
        reward = -1;
    end
    
    dlX(agentLoc(1),agentLoc(2)) = 2;
end
