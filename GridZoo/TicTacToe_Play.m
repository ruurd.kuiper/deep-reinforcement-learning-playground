%% Test Network Using Custom Training Loop

%% Load Test Data
% Initiate the grid world.
clear all
close all
load('agent.mat');

bg = 0;
gridWorld = ones(3,3).*bg;
% Define the possible actions
numAgents = 2;
numActions = 9;
stopCondition = false;

%% Test on a GPU if one is available.
executionEnvironment = "cpu";

% Run a test
% Loop over episodes.
maxSteps = 5;

% Convert gridWorld to dlarray.
dlX = dlarray(single(gridWorld),'SSCB');

% If training on a GPU, then convert data to gpuArray.
if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
    dlX = gpuArray(dlX);
end

stopCondition = false;
for p = 1:numAgents
    agent(p).experience = [];
end

playerOrder = randperm(2);

% figure; hold on
% dlX(randi(3),randi(3),:,:) = playerOrder(2);

for step = 1:maxSteps
    % For each player shuffle the player order between matches
    
    for p = playerOrder
        %         imagesc(extractdata(dlX))
        if p == 1
            extractdata(dlX)
            % Predict a move using the actor network
            action = softmax(extractdata(forward(agent(1).actor,dlX)));
            
            % Randomly select an action based on the output probabilities
%             move = randsample(numActions, 1, true, action);
            [~,move] = max(action);
            
            
            % Save the current state (1), the action taken (2), the softmax of
            % the action (5) and the prediction of the critic (6)
            agent(p).experience{step,1} = extractdata(dlX);
            agent(p).experience{step,2} = zeros(numActions,1);
            agent(p).experience{step,2}(move) = 1;
            agent(p).experience{step,5} = action;
            agent(p).experience{step,6} = extractdata(forward(agent(1).critic,dlX));
            
            % Change the playing field based on the action, only if the
            % action was legal (the player needs to learn the rules)
            [move(1),move(2)] = ind2sub(size(gridWorld),move);
            if dlX(move(1), move(2)) == bg
                dlX(move(1), move(2)) = p;
            end
            % Save the changed playing field
            agent(p).experience{step,4} = extractdata(dlX);
            
            %             imagesc(extractdata(dlX))
            
        elseif p == 2
            extractdata(dlX)
            while true
                x = input('Give the vertical location of your move: ');
                y = input('Give the horizontal location of your move: ');
                if dlX(x,y) ~= bg || x < 1 || x > 3 || y < 1 || y > 3
                    disp('That spot is not available!');
                else
                    dlX(x,y) = p;
                    break
                end
            end
            
        end
        
        % Check if the player has three in a row
        for j = 1:3
            X = extractdata(dlX);
            if all(X(:,j) == p) || all(X(j,:) == p) || ...
                    isequal(X(1,1),X(2,2),X(3,3),p) || ...
                    isequal(X(1,3),X(2,2),X(3,1),p)
                
                % Reward the player if he has won and break the loop
                disp(['Player ' num2str(p) ' has won the game!']);
                stopCondition = true;
                break
            end
        end
        
        % End the game if there are no more legal moves
        if all(X(:) ~= bg) && stopCondition == false
            disp(['It''s a draw!']);
            stopCondition = true;
        end
        
        if stopCondition == true
            maxSteps = step;
            %             imagesc(extractdata(dlX))
            extractdata(dlX)
            pause(1)
            break;
        end
    end
    
    
    
    if stopCondition == true
        maxSteps = step;
        break;
    end
end
