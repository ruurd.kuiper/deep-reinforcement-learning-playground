function [matrix,location] = newRandomLocation(matrix,layer,value)
%% Adds the value to a random empty location in the matrix and returns this location
location = [randi(size(matrix,1)),randi(size(matrix,2))];
while any(matrix(location(1),location(2),:) ~= 0)
    location = [randi(size(matrix,1)),randi(size(matrix,2))];
end
matrix(location(1),location(2),layer) = value;