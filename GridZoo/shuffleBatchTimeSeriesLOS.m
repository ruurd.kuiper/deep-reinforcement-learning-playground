function [exp_batch, G_batch, r_old_batch, D_batch]  = shuffleBatchTimeSeriesLOS(miniBatchSize, experience, G, D)
maxSteps = numel(G);
I = randperm(maxSteps,miniBatchSize);
% exp_batch = experience{I(1),7}(:,:,end-minSteps+1:end);
% for t = 1:miniBatchSize
%     exp_batch = cat(2,exp_batch,experience{I(t),7}(:,:,:));
%     G_batch(t) = G(I(t));
%     r_old_batch(:,t) = experience{I(t),5};
%     D_batch(:,t) = D(I(t)) .* experience{I(t),2};
% end
% exp_batch = exp_batch(:,2:end,:);

exp_batch = experience(I,7);
exp_batch = cat(2, exp_batch{:});
G_batch = G(I);
r_old_batch = experience(I,5);
r_old_batch = cat(2, r_old_batch{:});
D_batch = experience(I,2);
D_batch = cat(2,D_batch{:});
D_batch = D(I) .* D_batch;
