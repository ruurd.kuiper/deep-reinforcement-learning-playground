%% Train Network Using Custom Training Loop
% This example shows how to train a network that navigates a grid world.
%
% If |trainingOptions| does not provide the options you need (for example, a
% custom learning rate schedule), then you can define your own custom training
% loop using automatic differentiation.
%
% This example trains a network to classify handwritten digits with the _time-based
% decay_ learning rate schedule: for each iteration, the solver uses the learning
% rate given by $\rho_t =\frac{\rho_0 }{1+k\;t}$, where _t_ is the iteration number,
% $\rho_0$ is the initial learning rate, and _k_ is the decay.
%% Load Training Data
% Initiate the grid world.

clear all
close all
bg = 0;
gridWorld = ones(3,3).*bg;
% Define the possible actions
actions = [1 1; 1 2; 1 3; 2 1; 2 2; 2 3; 3 1; 3 2; 3 3];
numActions = size(actions,1);
maxSteps = 9;
numAgents = 2;
%% Define Network
% Define the network and specify the average image using the |'Mean'| option
% in the image input layer.

layers = [
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    fullyConnectedLayer(50, 'Name', 'fc0')
    leakyReluLayer('Name', 'relu2')
    fullyConnectedLayer(50, 'Name', 'fc1')
    leakyReluLayer('Name', 'relu3')
    fullyConnectedLayer(50, 'Name', 'fc2')
    leakyReluLayer('Name', 'relu4')
%     fullyConnectedLayer(50, 'Name', 'fc3')
%     leakyReluLayer('Name', 'relu5')
    fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
lgraphActor = layerGraph(layers);

layers = [
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    fullyConnectedLayer(50, 'Name', 'fc0')
    leakyReluLayer('Name', 'relu2')
    fullyConnectedLayer(50, 'Name', 'fc1')
    leakyReluLayer('Name', 'relu3')
    fullyConnectedLayer(50, 'Name', 'fc2')
    leakyReluLayer('Name', 'relu4')
%     fullyConnectedLayer(50, 'Name', 'fc3')
%     leakyReluLayer('Name', 'relu5')
    fullyConnectedLayer(1, 'Name', 'fcFinal')];
lgraphCritic = layerGraph(layers);

for p = 1:numAgents
    agent(p).actor = dlnetwork(lgraphActor);
    agent(p).critic = dlnetwork(lgraphCritic);
    
    agent(p).velocityActor = [];
    agent(p).velocityCritic = [];
end

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.001;
momentum = 0.9;
decay = 0.001;

%% Train on a GPU if one is available.

executionEnvironment = "cpu";
%% Train Model
% Train the model using a custom training loop.
%
% For each epoch, shuffle the data and loop over mini-batches of data. At the
% end of each epoch, display the training progress.
%
% For each mini-batch:
%%
% * Convert the labels to dummy variables.
% * Convert the data to |dlarray| objects with underlying type single and specify
% the dimension labels |'SSCB'| (spatial, spatial, channel, batch).
% * For GPU training, convert to |gpuArray| objects.
% * Evaluate the model gradients and loss using |dlfeval| and the |modelGradients|
% function.
% * Determine the learning rate for the time-based decay learning rate schedule.
% * Update the network parameters using the |sgdmupdate| function.
%%
% Initialize the training progress plot.

plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numAgents
        agent(p).lineLossActor = animatedline('Color','red');
        %         agent(p).lineLossCritic = animatedline('Color','blue');
        agent(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end
%%
% Train the network.

iteration = 0;
start = tic;
maxEpisodes = 100000;
gamma = 0.9;
lambda = 0.95;
averageReward = 0;
reward = 1; % The reward for winning
punish = -1;
beta = 0.1; % Increases exploration
eps = 0.1;


% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    maxSteps = 128;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    stopCondition = false;
    playOrder = randperm(2);
    
    for step = 1:maxSteps
        % For each player shuffle the player order between matches
        
        for p = playOrder
            
            if p == 2
                temp = dlX;
                temp(dlX == 1) = 2;
                temp(dlX == 2) = 1;
                dlX = temp;
            end
            
            if all(dlX(:) == 0)
                dlX(randi(3),randi(3),:,:) = 2;
            end
            
            % Initiate the player reward for this step at 0
            agent(p).experience{step,3} = 0;
            
            % Predict a move using the actor network
            action = softmax(extractdata(forward(agent(1).actor,dlX)));
            %             action(dlX ~= 3) = 1e-10;
            
            % Randomly select an action based on the output probabilities
            move = randsample(numActions, 1, true, action);
            
            % Save the current state (1), the action taken (2), the softmax of
            % the action (5) and the prediction of the critic (6)
            agent(p).experience{step,1} = extractdata(dlX);
            agent(p).experience{step,2} = zeros(numActions,1);
            agent(p).experience{step,2}(move) = 1;
            agent(p).experience{step,5} = action;
            agent(p).experience{step,6} = extractdata(forward(agent(1).critic,dlX));
            
            % Change the playing field based on the action, only if the
            % action was legal (the player needs to learn the rules)
            [move(1),move(2)] = ind2sub(size(gridWorld),move);
            if dlX(move(1), move(2)) == bg
                dlX(move(1), move(2)) = 1;
                agent(p).experience{step,3} = agent(p).experience{step,3} + 0;
            else
                agent(p).experience{step,3} =  agent(p).experience{step,3} + punish;
            end
            % Save the changed playing field
            agent(p).experience{step,4} = extractdata(dlX);
            
            % Check if the player has (or could have) three in a row
            
            X = agent(p).experience{step,4};
            for j = 1:3
                if all(X(:,j) == 1) || all(X(j,:) == 1) || ...
                        isequal(X(1,1),X(2,2),X(3,3),1) || ...
                        isequal(X(1,3),X(2,2),X(3,1),1)
                    
                    %% Reward the player if he has won and break the loop
                    agent(p).experience{step,3} = agent(p).experience{step,3} + reward;
                    agent(3-p).experience{end,3} = agent(3-p).experience{end,3} - reward;
                    stopCondition = true;
                    break
                elseif all(X(:) ~= bg)
                    %% Reward both players half points for a draw
                    agent(p).experience{step,3} = agent(p).experience{step,3} + 0.5 * reward;
                    agent(3-p).experience{end,3} = agent(3-p).experience{end,3} + 0.5 * reward;
                    stopcondition = true;
                    break
%                 else
%                     %% Check if winning move was possible but not performed
%                     pX = X;
%                     pX(move(1),move(2)) = bg;
%                     for k = 1:3
%                         for l = 1:3
%                             if pX(k,l) == bg && ~(k == move(1) && l == move(2))
%                                 pX(k,l) = 1;
%                                 if all(pX(:,j) == 1) || all(pX(j,:) == 1) || ...
%                                         isequal(pX(1,1),pX(2,2),pX(3,3),1) || ...
%                                         isequal(pX(1,3),pX(2,2),pX(3,1),1)
%                                     agent(p).experience{step,3} = agent(p).experience{step,3} - reward;
%                                 end
%                                 pX(k,l) = bg;
%                             end
%                         end
%                     end
                end
            end
            
            if p == 2
                temp = dlX;
                temp(dlX == 1) = 2;
                temp(dlX == 2) = 1;
                dlX = temp;
            end
            
            % End the game if there are no more legal moves
            if all(X(:) ~= bg)
                stopCondition = true;
            end
            
            if stopCondition == true
                stopCondition = false;
                dlX = dlarray(single(gridWorld),'SSCB');
                %                 break;
            end
        end
        %         if stopCondition == true
        % Convert gridWorld to dlarray.
        %             stopCondition = false;
        %             dlX = dlarray(single(gridWorld),'SSCB');
        %             maxSteps = step;
        %             break;
        %         end
    end
    
    for p = playOrder
        %% GENERALIZED ADVANTAGED ESTIMATOR
        [D, G] = GAE([agent(p).experience{:,3}],[agent(p).experience{:,6}],lambda,gamma);
        
        maxEpochs = 2;
        miniBatchSize = floor(maxSteps/maxEpochs);
        
        for K = 1:maxEpochs
            % Shuffle the batch of experiences
            [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatch(miniBatchSize, agent(p).experience, G, D);
            
            exp_batch = dlarray(single(exp_batch),'SSCB');
            
            % Calculate the gradient from the batch of experiences
            [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,agent(1).actor,exp_batch,D_batch,r_old_batch,eps,beta);
            [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,agent(1).critic,exp_batch,G_batch);
            
            % Update the network parameters using the SGDM optimizer.
            [agent(1).actor, agent(1).velocityActor] = sgdmupdate(agent(1).actor, gradientsActor, agent(1).velocityActor, learnRate, momentum);
            [agent(1).critic, agent(1).velocityCritic] = sgdmupdate(agent(1).critic, gradientsCritic, agent(1).velocityCritic, learnRate, momentum);
        end
        
        % Display the training progress.
        agent(p).rewards(i) = sum([agent(p).experience{:,3}]);
        averageReward = mean(agent(p).rewards(i-min(i-1,30):i));
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(agent(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            %             addpoints(agent(p).lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
            addpoints(agent(p).lineReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
    end
    if mod(i,100) == 0
        save('agent.mat','agent');
    end
    for p = 1:2
        agent(p).experience = [];
    end
end
