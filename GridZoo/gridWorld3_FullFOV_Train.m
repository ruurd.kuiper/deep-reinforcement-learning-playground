%% Train multiple agents in a gridWorld 
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld. 
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it 
% seems that the optimal solution might take longer if the network is to 
% shallow.
% - Adding a (small) punishment value for illegal moves can also help

clear all
close all
%% Initiate the gridWorld.
% Specify the gridWorld parameters

numActions = 4;
numAgents = 1;
numRewards = 1;
viewDistance = 0;
wallPixel = 5;
selfPixel = 1;
bgPixel = 2;
agentPixel = 3;
rewardPixel = 4;
rewardValue = 1;
punishValue = 0;
numChannels = 5;
worldSize = [5 5];
wall = padarray(zeros(worldSize),[viewDistance viewDistance],1);
gridWorld = zeros(worldSize(1)+viewDistance*2,worldSize(2)+viewDistance*2,numChannels);
gridWorld(:,:,wallPixel) = wall;
% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

viewSize = worldSize;

%% Specify Training Options
% Specify the training options.
numEpochs = 20;
initialLearnRate = 0.001;
momentum = 0.9;
decay = 0.01;
iteration = 0;
start = tic;
maxEpisodes = 100000;
gamma = 0.95;
lambda = 0.95;
eps = 0.2;

averageReward = 0;
maxSteps = 100;

miniBatchSize = 30;
maxEpochs = ceil(maxSteps/miniBatchSize);

%% Define Network
% Define the network and specify the average image using the |'Mean'| option
% in the image input layer.

pretrainedNetworks = false;
optimizer = 'adam';

if pretrainedNetworks == true
    pretrained = load('agentGridWorld_1_Agent_1_Reward.mat');
    for p = 1:numAgents
        agent(p).actor = pretrained.agent.actor;
        agent(p).critic = pretrained.agent.critic;
    end
else
    layers = [
        %     imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'rescale-symmetric', 'Min', 0, 'Max', rewardValue)
        imageInputLayer([viewSize numChannels], 'Name', 'input', 'Normalization', 'none')
        fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc0')
        reluLayer('Name', 'relu2')
        fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc1')
        reluLayer('Name', 'relu3')
        fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
    lgraphActor = layerGraph(layers);
    
    layers = [
        %     imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'rescale-symmetric', 'Min', 0, 'Max', rewardValue)
        imageInputLayer([viewSize numChannels], 'Name', 'input', 'Normalization', 'none')
        fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc0')
        reluLayer('Name', 'relu2')
        fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc1')
        reluLayer('Name', 'relu3')
        fullyConnectedLayer(1, 'Name', 'fcFinal')];
    lgraphCritic = layerGraph(layers);
    
    for p = 1:numAgents
        agent(p).actor = dlnetwork(lgraphActor);
        agent(p).critic = dlnetwork(lgraphCritic);
    end
end

for p = 1:numAgents
    if strcmp(optimizer,'sgdm')
    agent(p).velocityActor = [];
    agent(p).velocityCritic = [];
    elseif strcmp(optimizer,'adam')
    agent(p).averageGradActor = [];
    agent(p).averageSqGradActor = [];
    agent(p).averageGradCritic = [];
    agent(p).averageSqGradCritic = [];
    end
end

%% Train on a GPU if one is available.

executionEnvironment = "cpu";
%% Train Model
% Train the model using a custom training loop.
%
% For each epoch, shuffle the data and loop over mini-batches of data. At the
% end of each epoch, display the training progress.
%
% For each mini-batch:
%%
% * Convert the labels to dummy variables.
% * Convert the data to |dlarray| objects with underlying type single and specify
% the dimension labels |'SSCB'| (spatial, spatial, channel, batch).
% * For GPU training, convert to |gpuArray| objects.
% * Evaluate the model gradients and loss using |dlfeval| and the |modelGradients|
% function.
% * Determine the learning rate for the time-based decay learning rate schedule.
% * Update the network parameters using the |sgdmupdate| function.
%%
% Initialize the training progress plot.

plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numAgents
        agent(p).lineLossActor = animatedline('Color','red');
        agent(p).lineLossCritic = animatedline('Color','blue');
        agent(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end

%% Train the network
% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    stopCondition = false;
    playOrder = randperm(numAgents);
    
    for p = 1:numAgents
        agent(p).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(agent(p).location(1),agent(p).location(2),:) ~= 0)
            agent(p).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(agent(p).location(1),agent(p).location(2),agentPixel) = 1;
    end
    
    for r = 1:numRewards
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
    end
    
    for step = 1:maxSteps
        % For each player shuffle the player order between matches
        for p = playOrder
            
            % Make a separate representation of the gridWorld for the
            % player
            agent(p).View = dlX;
            agent(p).View(agent(p).location(1),agent(p).location(2),selfPixel) = 1;
            
            % Predict a move using the actor network
            action = softmax(extractdata(gather(forward(agent(p).actor,agent(p).View))));
            
            % Randomly select an action based on the output probabilities
            move = randsample(numActions, 1, true, action);
            
            % Save the current state (1), the action taken (2), the softmax of
            % the action (5) and the prediction of the critic (6)
            agent(p).experience{step,1} = extractdata(gather(agent(p).View));
            agent(p).experience{step,2} = zeros(numActions,1);
            agent(p).experience{step,2}(move) = 1;
            agent(p).experience{step,5} = action;
            agent(p).experience{step,6} = extractdata(gather(forward(agent(p).critic,agent(p).View)));
            
            % Change the playing field based on the action
            [move(1),move(2)] = deal(actions(move,1),actions(move,2));
            
            % First check if the movement is within the playing field
            if all((agent(p).location + move) >= 1) && all((agent(p).location + move) <= size(gridWorld,1))
                % Then check if the movement is to an empty spot
                if all(dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2),:) == 0)
                    % Set previous location to zero
                    dlX(agent(p).location(1),agent(p).location(2),agentPixel) = 0;
                    % Set agent location to new location
                    agent(p).location = agent(p).location + move;
                    % Set new pixel to agent pixel value
                    dlX(agent(p).location(1),agent(p).location(2),agentPixel) = 1;
                    % Give zero reward
                    agent(p).experience{step,3} = 0;
                    
                    % If the movement is to a reward spot
                elseif dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2),rewardPixel) == 1
                    % Set reward pixel to zero
                    dlX(agent(p).location(1),agent(p).location(2),rewardPixel) = 0;
                    % Set agent location to new location
                    agent(p).location = agent(p).location + move;
                    % Set new pixel to agent pixel value
                    dlX(agent(p).location(1),agent(p).location(2),agentPixel) = 1;
                    % Reward the agent
                    agent(p).experience{step,3} = rewardValue;
                    
                    % Make new reward in the playing field at random
                    % location that is not already occupied
                    rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
                        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    end
                    dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
                    
                elseif dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2),wallPixel) == 1
                    % Do not move, punish the player
                    agent(p).experience{step,3} = punishValue;
                    % If the movement is to anything else (such as another
                    % player)
                elseif ~all(dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2),:) == 0)
                    % Do not move, give zero reward
                    agent(p).experience{step,3} = 0;
                end
            else % If move is illegal (outside of gridWorld)
                % Punish the agent
                agent(p).experience{step,3} = punishValue;
            end
            
            % Save the changed playing field
            agent(p).View = dlX;
            agent(p).View(agent(p).location(1),agent(p).location(2),selfPixel) = 1;
            agent(p).experience{step,4} = extractdata(agent(p).View);
        end
    end
    
    for p = 1:numAgents
        %% GENERALIZED ADVANTAGED ESTIMATOR
        [D, G] = GAE([agent(p).experience{:,3}],[agent(p).experience{:,6}],lambda,gamma);
        
        for K = 1:maxEpochs
            % Shuffle the batch of experiences
            [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatch(miniBatchSize, agent(p).experience, G, D);
            
            exp_batch = dlarray(single(exp_batch),'SSCB');
            
            % Calculate the gradient from the batch of experiences
            [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,agent(p).actor,exp_batch,D_batch,r_old_batch,eps);
            [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,agent(p).critic,exp_batch,G_batch);
            
            if strcmp(optimizer,'sgdm')
                % Update the network parameters using the SGDM optimizer.
                [agent(p).actor, agent(p).velocityActor] = sgdmupdate(agent(p).actor, gradientsActor, agent(p).velocityActor, learnRate, momentum);
                [agent(p).critic, agent(p).velocityCritic] = sgdmupdate(agent(p).critic, gradientsCritic, agent(p).velocityCritic, learnRate, momentum);
            elseif strcmp(optimizer,'adam')
                % Update the network parameters using the ADAM optimizer.
                [agent(p).actor,agent(p).averageGradActor,agent(p).averageSqGradActor] = adamupdate(agent(p).actor,gradientsActor,agent(p).averageGradActor,agent(p).averageSqGradActor,iteration);
                [agent(p).critic,agent(p).averageGradCritic,agent(p).averageSqGradCritic] = adamupdate(agent(p).critic,gradientsCritic,agent(p).averageGradCritic,agent(p).averageSqGradCritic,iteration);
            end
        end
        
        % Display the training progress.
        agent(p).rewards(i) = sum([agent(p).experience{:,3}]);
        averageReward = mean(agent(p).rewards(i-min(i-1,30):i));
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(agent(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            %             addpoints(agent(p).lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
            addpoints(agent(p).lineReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
        agent(p).experience = [];
    end
    
end
