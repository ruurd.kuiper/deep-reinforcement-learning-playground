%% Draw on sphere!
clear all
close all

figure;
colormap(lines)
fps = 60;
LOSrays = 10;
LOSdistance = 20;
vis = 'grid';

showLOS = false;

zooFile = 'zooWorld_20210114T210345_Final.mat'
while true
    switch vis
        case 'grid'
            %% Show as grid
            while true
                try
                    load(zooFile);
                    break
                end
                pause(1);
            end
            hold on
            for i = 1:length(zooWorld)
                z = flip(zooWorld{i},3);
                [M, I] = max(z,[],3);
                %                 if all(z(:,:,1) == z(:,:,2))
                %                     disp('hebbes')
                %                 end
                I(M == 0) = 0;
                %                 subplot(2,1,1)
                %                 imshow(ind2rgb(imresize(permute(I,[2 1])+1,1,'nearest'),lines),'InitialMagnification', 5000);
                imshow(ind2rgb(imresize(permute(I,[2 1])+1,1,'nearest'),lines));
                hold on
                
                if showLOS
                    [x,y] = find(zooWorld{i}(:,:,3)==1);
                    [sight,sightLine] = lineOfSight(zooWorld{i},[x y],LOSrays,LOSdistance);
                    for i = 1:LOSrays*4
                        %                     subplot(2,1,1)
                        
                        plot(sightLine{i}(:,1),sightLine{i}(:,2),'yellow')
                    end
                end
                drawnow
                pause(1/fps)
                cla
                %                 sight = reshape(sight,[],5)';
                %                 sight(1,:) = round(sight(1,:)*255/20);
                %                 sight(2:end,:) = round(sight(2:end,:)*255);
                %                 subplot(2,1,2), imshow(ind2rgb(imresize(sight,1,'nearest'),gray),'InitialMagnification',5000);
                
                %                 subplot(2,1,1), imshow(ind2rgb(imresize(I+1,1,'nearest'),lines),'InitialMagnification', 5000);
                %                 subplot(2,1,2), imshow(ind2rgb(imresize(sight+1,1,'nearest'),gray),'InitialMagnification',5000);
                %                 drawnow
                %                 pause(0.01)
            end
            cla
        case 'planet'
            %% Show as planet
            load('zooWorld.mat');
            [X,Y,Z] = sphere(size(zooWorld{1},1));
            zooPlanet = surfl(X,Y,Z,'light');
            zooPlanet(1).CData = zeros([size(zooPlanet(1).ZData)]);
            zooPlanet(1).EdgeColor = 'none';
            lighting gouraud
            axis off
            axis('vis3d')
            set(gcf,'Color','k')
            for i = 1:length(zooWorld)
                %         [~, zooPlanet.CData] = max(extractdata(zooWorld{i}),[],3);
                zooPlanet(1).CData = extractdata(zooWorld{i})+0.2;
                drawnow
                for j = 1:fps
                    a = 360/length(zooWorld)*(i+j/fps);
                    view([a sin((90+a)*pi/180)*90/pi]);
                    pause(0.002)
                end
            end
            cla
    end
end