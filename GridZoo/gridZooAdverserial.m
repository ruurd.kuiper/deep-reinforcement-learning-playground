%% Train multiple agents in a gridWorld with LSTMs
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld.
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it
% seems that the optimal solution might take longer if the network is to
% shallow.
% - Adding a (small) punishment value for illegal moves can also help
% - When the network is too big (too many nodes per layer) it will not
% converge

%% ZOO
% - In the zoo, all animals of the same species learn together

clear all
close all
%% Initiate the gridWorld.
% Specify the gridWorld parameters
iterationsSinceLastUpdate = 0;
worldSize = [10 10];
numActions = 4;
numSpecies = 2;
numAnimals = 2;
numRewards = ceil(worldSize(1)*worldSize(2)/15);
numWalls = 0;
numWallsUpdate = 1;

%% Set view to full world or smaller FOV
smallFOV = true;

viewDistance = 3;
agentPixel = 2;
rewardPixel = 2;
wallPixel = 1;
rewardValue = 1;
eatValue = 5;
punishValue = -1;
numChannels = 2 + numSpecies;

% Spawn settings
respawnReward = true;

% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.0001;
momentum = 0.9;
decay = 0.01;
iteration = 0;
start = tic;
maxEpisodes = 10000;
gamma = 0.95;
lambda = 0.99;
eps = 0.1;
beta = 0.1;

averageReward = 0;
maxSteps = 200;
minSteps = 10;

miniBatchSize = 100;
maxEpochs = 2;%ceil(maxSteps/miniBatchSize);

%% Network settings
executionEnvironment = "cpu";
pretrainedNetworks = false;
optimizer = 'adam';
lstmUnits = worldSize(1);%*worldSize(2);
fcUnits = worldSize(1);%*worldSize(2);
if smallFOV == true
    viewSize = [viewDistance*2+1 viewDistance*2+1 numChannels];
else
    viewSize = [worldSize numChannels];
end

%% Choose between pretrained or new network
if pretrainedNetworks == true
    pretrained = load('species.mat');
    for p = 1:numSpecies
        species(p).actor = pretrained.species.actor;
        species(p).critic = pretrained.species.critic;
        if strcmp(optimizer,'sgdm')
            species(p).velocityActor = pretrained.species(p).velocityActor;
            species(p).velocityCritic = pretrained.species(p).velocityCritic;
        elseif strcmp(optimizer,'adam')
            species(p).averageGradActor = pretrained.species(p).averageGradActor;
            species(p).averageSqGradActor = pretrained.species(p).averageSqGradActor;
            species(p).averageGradCritic = pretrained.species(p).averageGradCritic;
            species(p).averageSqGradCritic = pretrained.species(p).averageSqGradCritic;
        end
    end
else
    for p = 1:numSpecies
        
        layers = [
            sequenceInputLayer(viewSize, 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            lstmLayer(lstmUnits, 'Name', 'lstm', 'OutputMode', 'last')
            fullyConnectedLayer(fcUnits, 'Name', 'fc1')
            reluLayer('Name', 'relu1')
            %             fullyConnectedLayer(fcUnits, 'Name', 'fc2')
            %             reluLayer('Name', 'relu2')
            %             fullyConnectedLayer(fcUnits, 'Name', 'fc3')
            %             reluLayer('Name', 'relu3')
            fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
        lgraphActor = layerGraph(layers);
        
        layers = [
            sequenceInputLayer(viewSize, 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            lstmLayer(lstmUnits, 'Name', 'lstm', 'OutputMode', 'last')
            fullyConnectedLayer(fcUnits, 'Name', 'fc1')
            reluLayer('Name', 'relu1')
            %             fullyConnectedLayer(fcUnits, 'Name', 'fc2')
            %             reluLayer('Name', 'relu2')
            %             fullyConnectedLayer(25, 'Name', 'fc3')
            %             reluLayer('Name', 'relu3')
            fullyConnectedLayer(1, 'Name', 'fcFinal')];
        lgraphCritic = layerGraph(layers);
        
        species(p).actor = dlnetwork(lgraphActor);
        species(p).critic = dlnetwork(lgraphCritic);
    end
end

for p = 1:numSpecies
    if strcmp(optimizer,'sgdm')
        species(p).velocityActor = [];
        species(p).velocityCritic = [];
    elseif strcmp(optimizer,'adam')
        species(p).averageGradActor = [];
        species(p).averageSqGradActor = [];
        species(p).averageGradCritic = [];
        species(p).averageSqGradCritic = [];
    end
    savedSpecies(1).actor(p) = species(p).actor;
    savedSpecies(1).critic(p) = species(p).critic;
end

%% Plot the training
plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numSpecies
        species(p).lineAverageReward = animatedline('Color','red');
        species(p).lineLossActor = animatedline('Color','blue');
        species(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end

borderThickness = 1;
%% Train the network
% Loop over episodes.
for i = 1:maxEpisodes
    
    % Save the species for experience replay
    if i > 1
        if mod(i,10) == 0
            for p = 1:numSpecies
                savedSpecies(i/10+1).actor(p) = species(p).actor;
                savedSpecies(i/10+1).critic(p) = species(p).critic;
            end
        end
    end
    
    % Create the gridWorld
    gridWorld = zeros(worldSize(1),worldSize(2),numChannels);
    
    % Make the north and south pole
    gridWorld(1:borderThickness,:,wallPixel) = 1;
    gridWorld(end-borderThickness+1:end,:,wallPixel) = 1;
    gridWorld(:,1:borderThickness,wallPixel) = 1;
    gridWorld(:,end-borderThickness+1:end,wallPixel) = 1;
    
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    %% Switch up the order of players so there is no bias
    stopCondition = false;
    playOrderSpecies = randperm(numSpecies);
    playOrderAnimals = randperm(numAnimals);
    
    %% Give all players, rewards and walls a random spot on the map
    %     species(1).animal(1).location = [borderThickness borderThickness] + 1;
    %     species(2).animal(1).location = [worldSize(1) worldSize(2)] - viewDistance;
    for p = 1:numSpecies
        for a = 1:numAnimals
            species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            while any(dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),:) ~= 0)
                species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            end
            dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),agentPixel+p) = 1;
        end
    end
    
    for r = 1:numRewards
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
    end
    
    for r = 1:numWalls
        wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(wallLoc(1),wallLoc(2),:) == 1)
            wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(wallLoc(1),wallLoc(2),wallPixel) = 1;
    end
    
    for p = playOrderSpecies
        for a = playOrderAnimals
            
            %% Make a small FOV representation of the gridWorld for the player
            if smallFOV == true
                bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                
                if any(bb1<1)
                    bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                end
                if any(bb1>size(dlX,1))
                    bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                end
                if any(bb2<1)
                    bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                end
                if any(bb2>size(dlX,2))
                    bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                end
                
                species(p).animal(a).experience{1,1} = dlX(bb1,bb2,:);
                species(p).animal(a).experience{1,7} = dlarray(species(p).animal(a).experience{1,1},'SSCBT');
                %% Or use the full FOV
            else
                species(p).animal(a).experience{1,1} = dlX;
                species(p).animal(a).experience{1,7} = dlarray(species(p).animal(a).experience{1,1},'SSCBT');
            end
        end
    end
    
    for step = 1:maxSteps
        for p = 1:numSpecies
            %% This is for experience replay
            if i>1
                useSaved(p) = randsample([0 1],1,true,[0.95 0.05]);
            else
                useSaved(p) = 0;
            end
            if useSaved(p) == false
                currentActor(p) = species(p).actor;
                currentCritic(p) = species(p).critic;
            else
                randomSavedSpeciesNr = randi(length(savedSpecies));
                currentActor(p) = savedSpecies(randomSavedSpeciesNr).actor(p);
                currentCritic(p) = savedSpecies(randomSavedSpeciesNr).critic(p);
            end
            for a = 1:numAnimals
                %% This is to at least always give some reward
                species(p).animal(a).experience{step,3} = 0;
            end
        end
    end
    
    for step = 1:maxSteps
        for p = playOrderSpecies
            for a = playOrderAnimals
                
                if step > 1
                    %% Make a small FOV representation of the gridWorld for the player
                    if smallFOV == true
                        % Find the Field Of View of the animal
                        bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                        bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                        
                        % If the field of view is outside the world, wrap around
                        if any(bb1<1)
                            bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                        end
                        if any(bb1>size(dlX,1))
                            bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                        end
                        if any(bb2<1)
                            bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                        end
                        if any(bb2>size(dlX,2))
                            bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                        end
                        
                        % Save the field of view of this step in the experience
                        species(p).animal(a).experience{step,1} = dlX(bb1,bb2,:);
                        %% Or use the full FOV
                    else
                        species(p).animal(a).experience{step,1} = extractdata(dlX);
                    end
                    % Add the information of this step to the sequence input
                    species(p).animal(a).experience{step,7} = cat(5,species(p).animal(a).experience{step-1,7},species(p).animal(a).experience{step,1});
                    
                    % Put the information in a dlarray
                    if step < minSteps
                        species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7},'SSCBT');
                    else
                        species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7}(:,:,:,:,end-minSteps+1:end),'SSCBT');
                    end
                end
            end
            
            for a = playOrderAnimals
                % Predict a move using the actor network
                if step < minSteps
                    pred = extractdata(gather(forward(currentActor(p),species(p).animal(a).experience{step,7})));
                else
                    pred = extractdata(gather(forward(currentActor(p),species(p).animal(a).experience{step,7}(:,:,:,:,end-minSteps+1:end))));
                end
                
                % Softmax the prediction
                action = softmax(pred);
                
                % Randomly select an action based on the output probabilities
                move = randsample(numActions, 1, true, action);
                
                % Save the current state (1), the action taken (2), the softmax of
                % the action (5) and the prediction of the critic (6)
                species(p).animal(a).experience{step,2} = zeros(numActions,1);
                species(p).animal(a).experience{step,2}(move) = 1;
                species(p).animal(a).experience{step,5} = action;
                if step < minSteps
                    species(p).animal(a).experience{step,6} = extractdata(gather(forward(currentCritic(p),species(p).animal(a).experience{step,7})));
                else
                    species(p).animal(a).experience{step,6} = extractdata(gather(forward(currentCritic(p),species(p).animal(a).experience{step,7}(:,:,:,:,end-minSteps+1:end))));
                end
                
                
                oldLoc = species(p).animal(a).location;
                
                if move ~= 5
                    %% Change the playing field based on the action
                    [move(1),move(2)] = deal(actions(move,1),actions(move,2));
                    
                    newLoc = species(p).animal(a).location + move;
                    
                    if newLoc(1)<1
                        newLoc(1) = size(dlX,1) + newLoc(1);
                    elseif newLoc(1)>size(dlX,1)
                        newLoc(1) = newLoc(1) - size(dlX,1);
                    end
                    if newLoc(2)<1
                        newLoc(2) = size(dlX,2) + newLoc(2);
                    elseif newLoc(2)>size(dlX,2)
                        newLoc(2) = newLoc(2) - size(dlX,2);
                    end
                    
                    
                    %% Check if the movement is to an empty spot
                    if all(dlX(newLoc(1), newLoc(2),:) == 0)
                        % Set previous location to zero
                        dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        % Set agent location to new location
                        species(p).animal(a).location = newLoc;
                        % Set new pixel to species pixel value
                        dlX(newLoc(1),newLoc(2),agentPixel+p) = 1;
                        % Give zero reward
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + 0;
                        
                        %% If the movement is to a reward spot
                    elseif dlX(newLoc(1), newLoc(2),rewardPixel) == 1
                        % Set previous location to zero
                        dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        % Set agent location to new location
                        species(p).animal(a).location = newLoc;
                        % Set reward pixel to zero
                        dlX(newLoc(1),newLoc(2),rewardPixel) = 0;
                        % Reward the animal
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + rewardValue;
                        % Make new reward in the world at random free location
                        if respawnReward == true
                            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                            while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
                                rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                            end
                            dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
                        end
                        
                        % Set new pixel to species pixel value
                        dlX(newLoc(1),newLoc(2),agentPixel+p) = 1;
                        %% If the move is to another player pixel
                        %                     elseif dlX(newLoc(1), newLoc(2),4) ~= 0
                        %                         if p == 1
                        %                             % Set previous location to zero
                        %                             dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        %                             % Set agent location to new location
                        %                             species(p).animal(a).location = newLoc;
                        %                             % Set new pixel to species pixel value
                        %                             dlX(newLoc(1),newLoc(2),agentPixel+p) = 1;
                        %
                        %                             % Reward the species
                        %                             species(1).animal(1).experience{step,3} = species(1).animal(1).experience{step,3} + eatValue;
                        %                             % Punish the other species
                        %                             species(2).animal(1).experience{step,3} = species(2).animal(1).experience{step,3} - eatValue;
                        %                         end
                        
                        %% If the move is to a wall pixel
                    elseif dlX(newLoc(1), newLoc(2),wallPixel) ~= 0
                        % Do not move, punish the player
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                        % If the movement is to anything else (such as another
                        % player)
                    elseif ~all(dlX(newLoc(1), newLoc(2),:) == 0)
                        % Do not move, give zero reward
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + 0;
                    end
                elseif move == 5
                    %                     if dlX(oldLoc(1), oldLoc(2),wallPixel) == 0 && ...
                    %                             dlX(oldLoc(1), oldLoc(2),rewardPixel) == 0
                    %                         dlX(oldLoc(1), oldLoc(2),wallPixel) = 1;
                    %                         species(p).animal(a).experience{step,3} = 0;
                    %                     else
                    species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                    %                     end
                end
                
                % Save the changed playing field
                bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                
                if any(bb1<1)
                    bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                end
                if any(bb1>size(dlX,1))
                    bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                end
                if any(bb2<1)
                    bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                end
                if any(bb2>size(dlX,2))
                    bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                end
                
                %                 species(p).animal(a).experience{step,4} = extractdata(dlX(bb1,bb2,:));
                species(p).animal(a).experience{step,4} = dlX;
                
                if a == 1
                    zooWorld{step} = dlX;
                end
            end
        end
    end
    
    
    
    for p = playOrderSpecies
        if useSaved == false
            %% GENERALIZED ADVANTAGED ESTIMATOR
            for a = playOrderAnimals
                %% Calculate advantage
                [D, G] = GAE([species(p).animal(a).experience{:,3}],[species(p).animal(a).experience{:,6}],lambda,gamma);
                for K = 1:maxEpochs
                    % Shuffle the batch of experiences
                    [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatchTimeSeries(miniBatchSize, species(p).animal(a).experience, G, D, minSteps);
                    
                    exp_batch = dlarray(exp_batch,'SSCBT');
                    
                    % Calculate the gradient from the batch of experiences
                    [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,species(p).actor,exp_batch,D_batch,r_old_batch,eps,beta);
                    [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,species(p).critic,exp_batch,G_batch);
                    
                    if strcmp(optimizer,'sgdm')
                        % Update the network parameters using the SGDM optimizer.
                        [species(p).actor, species(p).velocityActor] = sgdmupdate(species(p).actor, gradientsActor, species(p).velocityActor, learnRate, momentum);
                        [species(p).critic, species(p).velocityCritic] = sgdmupdate(species(p).critic, gradientsCritic, species(p).velocityCritic, learnRate, momentum);
                    elseif strcmp(optimizer,'adam')
                        % Update the network parameters using the ADAM optimizer.
                        [species(p).actor,species(p).averageGradActor,species(p).averageSqGradActor] = adamupdate(species(p).actor,gradientsActor,species(p).averageGradActor,species(p).averageSqGradActor,iteration);
                        [species(p).critic,species(p).averageGradCritic,species(p).averageSqGradCritic] = adamupdate(species(p).critic,gradientsCritic,species(p).averageGradCritic,species(p).averageSqGradCritic,iteration);
                    end
                end
            end
        end
        
        %% Display the training progress.
        species(p).rewards(i) = sum([species(p).animal(a).experience{:,3}]);
        averageReward = mean(species(p).rewards(i-min(i-1,30):i));
        if iterationsSinceLastUpdate > 100
            if averageReward >= 0.5*maxSteps*rewardValue
                %                         numWalls = numWalls+numWallsUpdate;
                worldSize = worldSize;
                iterationsSinceLastUpdate = 0;
            end
        end
        iterationsSinceLastUpdate = iterationsSinceLastUpdate + 1;
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(species(p).lineReward,iteration,species(p).rewards(end))
            addpoints(species(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            addpoints(species(p).lineAverageReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
        species(p).animal(a).experience = [];
    end
    
    save('zooWorld2.mat','zooWorld');
end
