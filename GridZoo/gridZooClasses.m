%% Train multiple agents in a gridWorld with LSTMs
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld.
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it
% seems that the optimal solution might take longer if the network is to
% shallow.
% - Adding a (small) punishment value for illegal moves can also help
% - When the network is too big (too many nodes per layer) it will not
% converge

%% ZOO
% - In the zoo, all animals of the same species learn together

clear all
close all

%% Name:
datename = datestr(datetime('now'),30);

%% Initiate the gridWorld.
% Specify the gridWorld parameters
iterationsSinceLastUpdate = 0;
worldSize = [100 35];
numActions = 4;
numSpecies = 2;
numAnimals = 1;
numRewards = 1;
numWalls = 0;
numWallsUpdate = 1;

%% Animal settings
max_speed = 8;
min_speed = 1;
animal_radius = 3;
speed_changerate = 2;
drag = 0.02;


%% Set view to full world or smaller FOV
smallFOV = true;

% viewDistance = 3;
agentPixel = 2; % (+ the number of the species)
rewardPixel = 2;
wallPixel = 1;
borderThickness = 0;
rewardValue = 1;
standardReward = 0;
eatValue = 5;
punishValue = -1;
numChannels = 2 + numSpecies;

LOSrays = 16;
LOSrange = 50;

% Spawn settings
respawnReward = false;

% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.001;
decay = 0.001;
iteration = 0;
start = tic;
maxEpisodes = 10000;
gamma = 0.99;
lambda = 0.95;
eps = 0.2;
beta = 0.01;

averageReward = 0;
maxSteps = 1024;
minSteps = 20;

maxEpochs = 2;%ceil(maxSteps/miniBatchSize);
miniBatchSize = round(maxSteps*numAnimals/2);

experienceReplay = true;
expReplayAmount = 0.4;
episodesPerExpReplaySave = 20;

%% Network settings
executionEnvironment = "cpu";
pretrainedNetworks = false;
optimizer = 'adam';

inputType = 'coords';

%% Create the species
for i = 1:numSpecies
    species(i) = Species(numAnimals);
    for j = 1:numAnimals
        gameObjects{numAnimals*(i-1)+j} = species(i).animal(j);
        species(i).animal(j).layer = 2+i;
    end
    savedSpecies(1).actor(i) = species(i).actor;
    savedSpecies(1).critic(i) = species(i).critic;
end

%% Create the rewards
for i = 1:numRewards
    rewards(i) = Reward;
    gameObjects{end+1} = rewards(i);
end

%% Train the network
% Loop over episodes.
for ep = 1:maxEpisodes
    %     profile on
    % Save the species for experience replay
    if ep > 1
        if mod(ep,episodesPerExpReplaySave) == 0
            for p = 1:numSpecies
                savedSpecies(ep/episodesPerExpReplaySave+1).actor(p) = species(p).actor;
                savedSpecies(ep/episodesPerExpReplaySave+1).critic(p) = species(p).critic;
            end
        end
    end
    
    % Create the gridWorld
    gridWorld = zeros(worldSize(1),worldSize(2),numChannels);
    
    % Draw the walls
    gridWorld(1:borderThickness,:,wallPixel) = 1;
    gridWorld(end-borderThickness+1:end,:,wallPixel) = 1;
    gridWorld(:,1:borderThickness,wallPixel) = 1;
    gridWorld(:,end-borderThickness+1:end,wallPixel) = 1;
    
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = gridWorld;
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || (executionEnvironment == "gpu" && canUseGPU)
        dlX = gpuArray(dlX);
    end
    
    %% Switch up the order of players so there is no bias
    stopCondition = false;
    playOrderSpecies = randperm(numSpecies);
    playOrderAnimals = randperm(numAnimals);
    
    %% Give all players, rewards and walls a random spot on the map
    gameObjects{1}.x = round(size(dlX,1) * 1/8);
    gameObjects{2}.x = round(size(dlX,1) * 7/8);
    gameObjects{3}.x = round(size(dlX,1)/2);
    gameObjects{3}.y = round(size(dlX,2)/2);
    gameObjects{3}.vx = 0;
    gameObjects{3}.vy = 0;
    for i = 1:2
        while true
%             gameObjects{i}.x = randi(size(gridWorld,1));
            gameObjects{i}.y = randi(size(gridWorld,2));
            if checkValidPos(dlX,gameObjects{i},gameObjects) == true && ~isnan(gameObjects{i}.x)
                break
            end
        end
        gameObjects{i}.vx = 0;
        gameObjects{i}.vy = 0;
    end
    
    %% Draw all objects into the map
    for i = 1:length(gameObjects)
        dlX = drawObjectInMap(dlX,gameObjects{i});
    end
    
    %% Initiate the players with a starting memory
    dlX_compressed = dlX(:,:,1:2);
    dlX_compressed(:,:,3) = sum(dlX(:,:,3:end),3);
    for p = playOrderSpecies
        for a = playOrderAnimals
            switch inputType
                case 'lineOfSight'
                    dlX_input = lineOfSightAngular(dlX_compressed,[species(p).animal(a).x, species(p).animal(a).y],LOSrays,LOSrange,species(p).animal(a).shape);
                case 'map'
                    dlX_input = dlX(1:5:end,1:5:end,2:end);
                case 'coords'
                    dlX_input = [];
                    for i = 1:length(gameObjects)
                        dlX_input(end+1) = gameObjects{i}.x/size(dlX,1);
                        dlX_input(end+1) = gameObjects{i}.y/size(dlX,2);
                        dlX_input(end+1) = gameObjects{i}.vx/max_speed;
                        dlX_input(end+1) = gameObjects{i}.vy/max_speed;
                    end
            end
            species(p).animal(a).experience{1,1} = dlX_input(:);
            species(p).animal(a).experience{1,7} = dlarray(repmat(species(p).animal(a).experience{1,1},[1,1,minSteps]),'CBT');
        end
    end
    
    %% This is for experience replay
    % (only if there is more than one species)
    for p = 1:numSpecies
        for a = 1:numAnimals
            currentActor(p,a) = species(p).actor;
            currentCritic(p,a) = species(p).critic;
        end
    end
    
    useSaved = zeros(p,a);
    
    for p = 1:numSpecies
        for a = 1:numAnimals
            if experienceReplay && sum(useSaved(:)) < 1 && ep > 1
                useSaved(p,a) = randsample([0 1],numAnimals,1,[1-expReplayAmount expReplayAmount]);
                if useSaved(p,a)
                    randomSavedSpeciesNr = randi(length(savedSpecies));
                    currentActor(p,a) = savedSpecies(randomSavedSpeciesNr).actor(p);
                    currentCritic(p,a) = savedSpecies(randomSavedSpeciesNr).critic(p);
                end
            end
        end
    end
    
    for step = 1:maxSteps
        
        %% Clear the grid and redraw all objects
        dlX = gridWorld;
        for i = 1:length(gameObjects)
            dlX = drawObjectInMap(dlX,gameObjects{i});
        end
        
        %% This is to at least always give some reward
        for p = playOrderSpecies
            for a = playOrderAnimals
                species(p).animal(a).experience{step,3} = standardReward;
            end
        end
        
        dlX_compressed = dlX(:,:,1:2);
        dlX_compressed(:,:,3) = sum(dlX(:,:,3:end),3);
        for p = 1:numSpecies
            for a = 1:numAnimals
                if step > 1
                    %% Make a small FOV representation of the gridWorld for the player
                    switch inputType
                        case 'lineOfSight'
                            dlX_input = lineOfSightAngular(dlX_compressed,[species(p).animal(a).x, species(p).animal(a).y],LOSrays,LOSrange,species(p).animal(a).shape);
                        case 'map'
                            dlX_input = dlX(1:5:end,1:5:end,2:end);
                        case 'coords'
                            dlX_input = [];
                            for i = 1:length(gameObjects)
                                dlX_input(end+1) = gameObjects{i}.x/size(dlX,1);
                                dlX_input(end+1) = gameObjects{i}.y/size(dlX,2);
                                dlX_input(end+1) = gameObjects{i}.vx/max_speed;
                                dlX_input(end+1) = gameObjects{i}.vy/max_speed;
                            end
                    end
                    species(p).animal(a).experience{step,1} = dlX_input(:);
                    % Add the information of this step to the sequence input
                    species(p).animal(a).experience{step,7} = cat(3,species(p).animal(a).experience{step-1,7},species(p).animal(a).experience{step,1});
                    species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7}(:,:,end-minSteps+1:end),'CBT');
                end
                while true
                    % Try catch in a loop, to load a past working
                    % actor/critic if the current one has broken
                    try
                        % Predict a move using the actor network
                        pred = extractdata(gather(forward(currentActor(p,a),species(p).animal(a).experience{step,7})));
                        % Softmax the prediction
                        action = softmax(pred);
                        
                        % Randomly select an action based on the output probabilities
                        move = randsample(numActions, 1, true, action);
                        break
                    catch
                        pause(3)
                        pretrained = load(['species' num2str(p) '_' datename '.mat']);
                        pause(3)
                        species(p).actor = pretrained.savedSpeciesToFile.actor;
                        species(p).critic = pretrained.savedSpeciesToFile.critic;
                        
                        species(p).averageGradActor = pretrained.savedSpeciesToFile.averageGradActor;
                        species(p).averageSqGradActor = pretrained.savedSpeciesToFile.averageSqGradActor;
                        species(p).averageGradCritic = pretrained.savedSpeciesToFile.averageGradCritic;
                        species(p).averageSqGradCritic = pretrained.savedSpeciesToFile.averageSqGradCritic;
                        
                        currentActor(p,a) = species(p).actor;
                        currentCritic(p,a) = species(p).critic;
                        break
                    end
                end
                
                % Save the current state (1), the action taken (2), the softmax of
                % the action (5) and the prediction of the critic (6)
                species(p).animal(a).experience{step,2} = zeros(numActions,1);
                species(p).animal(a).experience{step,2}(move) = 1;
                species(p).animal(a).experience{step,5} = action;
                species(p).animal(a).experience{step,6} = extractdata(gather(forward(currentCritic(p,a),species(p).animal(a).experience{step,7}(:,:,end-minSteps+1:end))));
                
                switch move
                    case 1
                        species(p).animal(a).vx = species(p).animal(a).vx + speed_changerate;
                    case 2
                        species(p).animal(a).vx = species(p).animal(a).vx - speed_changerate;
                    case 3
                        species(p).animal(a).vy = species(p).animal(a).vy + speed_changerate;
                    case 4
                        species(p).animal(a).vy = species(p).animal(a).vy - speed_changerate;
                    case 5
                end
                
                speed = sqrt(species(p).animal(a).vx^2 + species(p).animal(a).vy^2);
                if speed > max_speed
                    species(p).animal(a).vx = species(p).animal(a).vx * max_speed/speed;
                    species(p).animal(a).vy = species(p).animal(a).vy * max_speed/speed;
                end
                
                % Update x and y
                species(p).animal(a).x = round(species(p).animal(a).x + species(p).animal(a).vx);
                species(p).animal(a).y = round(species(p).animal(a).y + species(p).animal(a).vy);
                
            end
            
        end
        % Detect collisions between objects
        detectCollisions(gameObjects);
        
        % Update x and y of the non-animal objects
        for i = numSpecies*numAnimals+1:length(gameObjects)
            gameObjects{i}.vx = gameObjects{i}.vx * (1 - drag);
            gameObjects{i}.vy = gameObjects{i}.vy * (1 - drag);
            gameObjects{i}.x = round(gameObjects{i}.x + gameObjects{i}.vx);
            gameObjects{i}.y = round(gameObjects{i}.y + gameObjects{i}.vy);
        end
        
        % Detect edge collisions
        detectEdgeCollisions(gameObjects,dlX,step);
        
        % If NaN, break off training this session
        breakFlag = false;
        for i = 1:length(gameObjects)
            if isnan(gameObjects{i}.x) || isnan(gameObjects{i}.y) || isnan(gameObjects{i}.vx) || isnan(gameObjects{i}.vy)
            breakFlag = true;
            end
        end
        if breakFlag
            break
        end
        
        % Save for memory/displaying
        zooWorld{step} = dlX;
        
        % Update these each step
        newMaxSteps = step*numAnimals;
        newMinSteps = min(minSteps,step);
        newMiniBatchSize = round(newMaxSteps/2);
    end
    
    if breakFlag
        continue
    end
    
    for p = 1:numSpecies
        species(p).experience = {};
        for a = 1:numAnimals
            if ~useSaved(p,a)
                species(p).experience = cat(1,species(p).experience,species(p).animal(a).experience);
            end
        end
    end
    
    for p = 1:numSpecies
        if useSaved(p) == false
            %% GENERALIZED ADVANTAGED ESTIMATOR
            %% Calculate advantage
            [D, G] = GAE([species(p).experience{:,3}],[species(p).experience{:,6}],lambda,gamma);
            for K = 1:maxEpochs
                % Shuffle the batch of experiences
                [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatchTimeSeriesLOS(newMiniBatchSize, species(p).experience, G, D);
                
                exp_batch = dlarray(exp_batch,'CBT');
                
                % Calculate the gradient from the batch of experiences
                [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,species(p).actor,exp_batch,D_batch,r_old_batch,eps,beta);
                [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,species(p).critic,exp_batch,G_batch);
                
                
                % Update the network parameters using the ADAM optimizer.
                [species(p).actor,species(p).averageGradActor,species(p).averageSqGradActor] = adamupdate(species(p).actor,gradientsActor,species(p).averageGradActor,species(p).averageSqGradActor,iteration,learnRate);
                [species(p).critic,species(p).averageGradCritic,species(p).averageSqGradCritic] = adamupdate(species(p).critic,gradientsCritic,species(p).averageGradCritic,species(p).averageSqGradCritic,iteration,learnRate);
            end
        end
        
        %% Display the training progress.
        species(p).rewards(ep) = 0;
        if useSaved(p) && ep > 1
            species(p).rewards(ep) = species(p).rewards(ep-1);
        else
            for a = playOrderAnimals
                species(p).rewards(ep) = species(p).rewards(ep) + sum([species(p).animal(a).experience{:,3}]);
            end
        end
        species(p).averageReward(ep) = mean(species(p).rewards(ep-min(ep-1,30):ep));
        
        %% Draw training plot
        Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
        addpoints(species(p).lineReward,iteration,species(p).rewards(end))
        addpoints(species(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
        addpoints(species(p).lineAverageReward,iteration,species(p).averageReward(ep))
        title("Elapsed: " + string(Dur))
        drawnow
        
        if all(species(p).averageReward(1:end-1) <=  species(p).averageReward(end))
            savedSpeciesToFile = species(p);
            save(['species' num2str(p) '_' datename '.mat'],'savedSpeciesToFile');
        end
        
    end
    
    save(['zooWorld_' datename '_Final.mat'],'zooWorld');
    
    %% Reset all experiences for the next episode
     for p = playOrderSpecies
        for a = playOrderAnimals
            species(p).animal(a).experience = {};
        end
    end
    
    %     profile viewer
    
end


