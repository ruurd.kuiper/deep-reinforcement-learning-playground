%% Train multiple agents in a gridWorld with LSTMs
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld.
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it
% seems that the optimal solution might take longer if the network is to
% shallow.
% - Adding a (small) punishment value for illegal moves can also help
% - When the network is too big (too many nodes per layer) it will not
% converge

%% ZOO
% - In the zoo, all animals of the same species learn together

clear all
close all
%% Initiate the gridWorld.
% Specify the gridWorld parameters
iterationsSinceLastUpdate = 0;
numActions = 4;
numSpecies = 1;
numAnimals = 1;
numRewards = 50;
numWalls = 50;
numWallsUpdate = 1;
viewDistance = 5;
agentPixel = 1;
rewardPixel = 2;
wallPixel = 3;
rewardValue = 1;
punishValue = -0.1;
numChannels = 3;
worldSize = [30 30];
gridWorld = zeros(worldSize(1),worldSize(2),numChannels);

% Make the north and south pole
gridWorld(1:viewDistance,:,wallPixel) = 1;
gridWorld(end-viewDistance:end,:,wallPixel) = 1;

% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.001;
momentum = 0.9;
decay = 0.01;
iteration = 0;
start = tic;
maxEpisodes = 100000;
gamma = 0.95;
lambda = 0.95;
eps = 0.2;

averageReward = 0;
maxSteps = 120;
minSteps = 30;

miniBatchSize = 40;
maxEpochs = ceil(maxSteps/miniBatchSize);

%% Define Network
% Define the network and specify the average image using the |'Mean'| option
% in the image input layer.

pretrainedNetworks = false;
optimizer = 'adam';

if pretrainedNetworks == true
    pretrained = load('zooWorld_1_Agent.mat');
    for p = 1:numSpecies
        species(p).actor = pretrained.species.actor;
        species(p).critic = pretrained.species.critic;
    end
else
    for p = 1:numSpecies
        viewSize = viewDistance*2+1;
        layers = [
            sequenceInputLayer([viewSize viewSize numChannels], 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            fullyConnectedLayer(400, 'Name', 'fc1')
%             flattenLayer('Name','flat')
            reluLayer('Name', 'relu1')
            lstmLayer(100, 'Name', 'lstm', 'OutputMode', 'last')
%             fullyConnectedLayer(100, 'Name', 'fc2')
            reluLayer('Name', 'relu2')
            fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
        lgraphActor = layerGraph(layers);
        
        layers = [
            sequenceInputLayer([viewSize viewSize numChannels], 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            fullyConnectedLayer(400, 'Name', 'fc1')
%             flattenLayer('Name','flat')
            reluLayer('Name', 'relu1')
            lstmLayer(100, 'Name', 'lstm', 'OutputMode', 'last')
%             fullyConnectedLayer(100, 'Name', 'fc2')
            reluLayer('Name', 'relu2')
            fullyConnectedLayer(1, 'Name', 'fcFinal')];
        lgraphCritic = layerGraph(layers);
        
        species(p).actor = dlnetwork(lgraphActor);
        species(p).critic = dlnetwork(lgraphCritic);
    end
end

for p = 1:numSpecies
    if strcmp(optimizer,'sgdm')
        species(p).velocityActor = [];
        species(p).velocityCritic = [];
    elseif strcmp(optimizer,'adam')
        species(p).averageGradActor = [];
        species(p).averageSqGradActor = [];
        species(p).averageGradCritic = [];
        species(p).averageSqGradCritic = [];
    end
end

%% Train on a GPU if one is available.

executionEnvironment = "cpu";
%% Train Model
% Train the model using a custom training loop.
%
% For each epoch, shuffle the data and loop over mini-batches of data. At the
% end of each epoch, display the training progress.
%
% For each mini-batch:
%%
% * Convert the labels to dummy variables.
% * Convert the data to |dlarray| objects with underlying type single and specify
% the dimension labels |'SSCBT'| (spatial, spatial, channel, batch, time).
% * For GPU training, convert to |gpuArray| objects.
% * Evaluate the model gradients and loss using |dlfeval| and the |modelGradients|
% function.
% * Determine the learning rate for the time-based decay learning rate schedule.
% * Update the network parameters using the |sgdmupdate| function.
%%
% Initialize the training progress plot.

plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numSpecies
        species(p).lineLossActor = animatedline('Color','red');
        species(p).lineLossCritic = animatedline('Color','blue');
        species(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end

%% Train the network
% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    stopCondition = false;
    playOrderSpecies = randperm(numSpecies);
    playOrderAnimals = randperm(numAnimals);
    
    for p = 1:numSpecies
        for a = 1:numAnimals
            species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            while any(dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),:) ~= 0)
                species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            end
            dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),agentPixel) = 1;
        end
    end
    
    for r = 1:numRewards
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
    end
    
    for r = 1:numWalls
        wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(wallLoc(1),wallLoc(2),:) == 1)
            wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(wallLoc(1),wallLoc(2),wallPixel) = 1;
    end
    
    % Make a separate representation of the gridWorld for the
    % player
    for p = playOrderSpecies
        for a = playOrderAnimals
            bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
            bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
            
            if any(bb1<1)
                bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
            end
            if any(bb1>size(dlX,1))
                bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
            end
            if any(bb2<1)
                bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
            end
            if any(bb2>size(dlX,2))
                bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
            end
            
            species(p).animal(a).experience{1,1} = dlX(bb1,bb2,:);
            species(p).animal(a).experience{1,7} = dlarray(species(p).animal(a).experience{1,1},'SSCBT');
        end
    end
    
    for step = 1:maxSteps
        % For each player shuffle the player order between matches
        zooWorld{step} = dlX;
        
        for p = playOrderSpecies
            for a = playOrderAnimals
                
                if step > 1
                    bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                    bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                    
                    if any(bb1<1)
                        bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                    end
                    if any(bb1>size(dlX,1))
                        bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                    end
                    if any(bb2<1)
                        bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                    end
                    if any(bb2>size(dlX,2))
                        bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                    end
                    
                    species(p).animal(a).experience{step,1} = extractdata(dlX(bb1,bb2,:));
                    species(p).animal(a).experience{step,7} = cat(5,species(p).animal(a).experience{step-1,7},species(p).animal(a).experience{step,1});
                    %                 species(p).animal(a).experience{step,7} = species(p).animal(a).experience{step,1};
                    species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7},'SSCBT');
                end
                
                % Predict a move using the actor network
                if step < minSteps
                    action = softmax(extractdata(gather(forward(species(p).actor,...
                        species(p).animal(a).experience{step,7}))));
                else
                    action = softmax(extractdata(gather(forward(species(p).actor,...
                        species(p).animal(a).experience{step,7}(:,:,:,:,end-minSteps+1:end)))));
                end
                
                % Randomly select an action based on the output probabilities
                move = randsample(numActions, 1, true, action);
                
                % Save the current state (1), the action taken (2), the softmax of
                % the action (5) and the prediction of the critic (6)
                species(p).animal(a).experience{step,2} = zeros(numActions,1);
                species(p).animal(a).experience{step,2}(move) = 1;
                species(p).animal(a).experience{step,5} = action;
                species(p).animal(a).experience{step,6} = extractdata(gather(forward(species(p).critic,species(p).animal(a).experience{step,7})));
                
                % Change the playing field based on the action
                [move(1),move(2)] = deal(actions(move,1),actions(move,2));
                
                oldLoc = species(p).animal(a).location;
                newLoc = species(p).animal(a).location + move;
                
                if newLoc(1)<1
                    newLoc(1) = size(dlX,1) + newLoc(1);
                elseif newLoc(1)>size(dlX,1)
                    newLoc(1) = newLoc(1) - size(dlX,1);
                end
                if newLoc(2)<1
                    newLoc(2) = size(dlX,2) + newLoc(2);
                elseif newLoc(2)>size(dlX,2)
                    newLoc(2) = newLoc(2) - size(dlX,2);
                end
                
                
                % Check if the movement is to an empty spot
                if all(dlX(newLoc(1), newLoc(2),:) == 0)
                    % Set previous location to zero
                    dlX(oldLoc(1),oldLoc(2),agentPixel) = 0;
                    % Set agent location to new location
                    species(p).animal(a).location = newLoc;
                    % Set new pixel to species pixel value
                    dlX(newLoc(1),newLoc(2),agentPixel) = p/numSpecies;
                    % Give zero reward
                    species(p).animal(a).experience{step,3} = 0;
                    
                    % If the movement is to a reward spot
                elseif dlX(newLoc(1), newLoc(2),rewardPixel) == 1
                    % Set previous location to zero
                    dlX(oldLoc(1),oldLoc(2),agentPixel) = 0;
                    % Set agent location to new location
                    species(p).animal(a).location = newLoc;
                    % Set reward pixel to zero
                    dlX(newLoc(1),newLoc(2),rewardPixel) = 0;
                    % Set new pixel to species pixel value
                    dlX(newLoc(1),newLoc(2),agentPixel) = p/numSpecies;
                    % Reward the animal
                    species(p).animal(a).experience{step,3} = rewardValue;
                    
                    % Make new reward in the playing field at random
                    % location that is not already occupied
                    rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
                        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    end
                    dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
                    
                elseif dlX(newLoc(1), newLoc(2),wallPixel) ~= 0
                    % Do not move, punish the player
                    species(p).animal(a).experience{step,3} = punishValue;
                    % If the movement is to anything else (such as another
                    % player)
                elseif ~all(dlX(newLoc(1), newLoc(2),:) == 0)
                    % Do not move, give zero reward
                    species(p).animal(a).experience{step,3} = 0;
                end
                
                % Save the changed playing field
                bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                
                if any(bb1<1)
                    bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                end
                if any(bb1>size(dlX,1))
                    bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                end
                if any(bb2<1)
                    bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                end
                if any(bb2>size(dlX,2))
                    bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                end
                
                species(p).animal(a).experience{step,4} = extractdata(dlX(bb1,bb2,:));
            end
        end
    end
    
    
    for p = playOrderSpecies
        %% GENERALIZED ADVANTAGED ESTIMATOR
        for a = playOrderAnimals
            [D, G] = GAE([species(p).animal(a).experience{:,3}],[species(p).animal(a).experience{:,6}],lambda,gamma);
            
            for K = 1:maxEpochs
                % Shuffle the batch of experiences
                [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatchTimeSeries(miniBatchSize, species(p).animal(a).experience, G, D, minSteps);
                
                exp_batch = dlarray(exp_batch,'SSCBT');
                
                % Calculate the gradient from the batch of experiences
                [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,species(p).actor,exp_batch,D_batch,r_old_batch,eps);
                [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,species(p).critic,exp_batch,G_batch);
                
                if strcmp(optimizer,'sgdm')
                    % Update the network parameters using the SGDM optimizer.
                    [species(p).actor, species(p).velocityActor] = sgdmupdate(species(p).actor, gradientsActor, species(p).velocityActor, learnRate, momentum);
                    [species(p).critic, species(p).velocityCritic] = sgdmupdate(species(p).critic, gradientsCritic, species(p).velocityCritic, learnRate, momentum);
                elseif strcmp(optimizer,'adam')
                    % Update the network parameters using the ADAM optimizer.
                    [species(p).actor,species(p).averageGradActor,species(p).averageSqGradActor] = adamupdate(species(p).actor,gradientsActor,species(p).averageGradActor,species(p).averageSqGradActor,iteration);
                    [species(p).critic,species(p).averageGradCritic,species(p).averageSqGradCritic] = adamupdate(species(p).critic,gradientsCritic,species(p).averageGradCritic,species(p).averageSqGradCritic,iteration);
                end
            end
        end
        
        % Display the training progress.
        species(p).rewards(i) = sum([species(p).animal(a).experience{:,3}]);
        averageReward = mean(species(p).rewards(i-min(i-1,30):i));
        if iterationsSinceLastUpdate > 5
            if averageReward >= 10
                numWalls = numWalls+numWallsUpdate;
                iterationsSinceLastUpdate = 0;
            end
        end
        iterationsSinceLastUpdate = iterationsSinceLastUpdate + 1;
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(species(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            %             addpoints(species(p).lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
            addpoints(species(p).lineReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
        species(p).animal(a).experience = [];
    end
    
    save('zooWorld.mat','zooWorld');
end
