%% Train Network Using Custom Training Loop
% This example shows how to train a network that navigates a grid world.
%
% If |trainingOptions| does not provide the options you need (for example, a
% custom learning rate schedule), then you can define your own custom training
% loop using automatic differentiation.
%
% This example trains a network to classify handwritten digits with the _time-based
% decay_ learning rate schedule: for each iteration, the solver uses the learning
% rate given by $\rho_t =\frac{\rho_0 }{1+k\;t}$, where _t_ is the iteration number,
% $\rho_0$ is the initial learning rate, and _k_ is the decay.
%% Load Training Data
% Initiate the grid world.

clear all
close all
numActions = 4;
numAgents = 2;
numRewards = 2;
bg = 1;
rewardValue = numAgents+bg+1;
gridWorld = ones(5,5).*bg;
% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Define Network
% Define the network and specify the average image using the |'Mean'| option
% in the image input layer.

layers = [
%     imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'rescale-symmetric', 'Min', 0, 'Max', rewardValue)
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc0')
    reluLayer('Name', 'relu2')
    fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc1')
    reluLayer('Name', 'relu3')
%     fullyConnectedLayer(25, 'Name', 'fc2')
%     reluLayer('Name', 'relu4')
    fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
lgraphActor = layerGraph(layers);

layers = [
%     imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'rescale-symmetric', 'Min', 0, 'Max', rewardValue)
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc0')
    reluLayer('Name', 'relu2')
    fullyConnectedLayer(numel(gridWorld)*4, 'Name', 'fc1')
    reluLayer('Name', 'relu3')
%     fullyConnectedLayer(25, 'Name', 'fc2')
%     reluLayer('Name', 'relu4')
    fullyConnectedLayer(1, 'Name', 'fcFinal')];
lgraphCritic = layerGraph(layers);

for p = 1:numAgents
    agent(p).actor = dlnetwork(lgraphActor);
    agent(p).critic = dlnetwork(lgraphCritic);
    
    agent(p).velocityActor = [];
    agent(p).velocityCritic = [];
end

%% Specify Training Options
% Specify the training options.
numEpochs = 20;
miniBatchSize = 128;
initialLearnRate = 0.001;
momentum = 0.9;
decay = 0.01;

%% Train on a GPU if one is available.

executionEnvironment = "cpu";
%% Train Model
% Train the model using a custom training loop.
%
% For each epoch, shuffle the data and loop over mini-batches of data. At the
% end of each epoch, display the training progress.
%
% For each mini-batch:
%%
% * Convert the labels to dummy variables.
% * Convert the data to |dlarray| objects with underlying type single and specify
% the dimension labels |'SSCB'| (spatial, spatial, channel, batch).
% * For GPU training, convert to |gpuArray| objects.
% * Evaluate the model gradients and loss using |dlfeval| and the |modelGradients|
% function.
% * Determine the learning rate for the time-based decay learning rate schedule.
% * Update the network parameters using the |sgdmupdate| function.
%%
% Initialize the training progress plot.

plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numAgents
        agent(p).lineLossActor = animatedline('Color','red');
        agent(p).lineLossCritic = animatedline('Color','blue');
        agent(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end
%%
% Train the network.

iteration = 0;
start = tic;
maxEpisodes = 100000;
gamma = 0.95;
lambda = 0.95;
averageReward = 0;
reward = 1; % The reward for winning

% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    maxSteps = 100;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    stopCondition = false;
    playOrder = randperm(numAgents);
    
    for p = 1:numAgents
        agent(p).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
%         agent(p).location = [p,p];
        while dlX(agent(p).location(1),agent(p).location(2)) ~= bg
            agent(p).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(agent(p).location(1),agent(p).location(2)) = p+bg;
    end
    
    for r = 1:numRewards
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while dlX(rewardLoc(1),rewardLoc(2)) ~= bg
            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(rewardLoc(1),rewardLoc(2)) = rewardValue;
    end
    
    for step = 1:maxSteps
        % For each player shuffle the player order between matches
        for p = playOrder
            
            agent(p).dlX = dlX;
            agent(p).dlX(dlX == p+bg) = 0;
            % Predict a move using the actor network
            action = softmax(extractdata(gather(forward(agent(p).actor,agent(p).dlX))));
            %             action(dlX ~= 3) = 0;
            
            % Randomly select an action based on the output probabilities
            move = randsample(numActions, 1, true, action);
            
            % Save the current state (1), the action taken (2), the softmax of
            % the action (5) and the prediction of the critic (6)
            agent(p).experience{step,1} = extractdata(gather(agent(p).dlX));
            agent(p).experience{step,2} = zeros(numActions,1);
            agent(p).experience{step,2}(move) = 1;
            agent(p).experience{step,5} = action;
            agent(p).experience{step,6} = extractdata(gather(forward(agent(p).critic,agent(p).dlX)));
            
            % Change the playing field based on the action, only if the
            % action was legal (the player needs to learn the rules)
            [move(1),move(2)] = deal(actions(move,1),actions(move,2));
            
            if all((agent(p).location + move) >= 1) && all((agent(p).location + move) <= size(gridWorld,1))
                if dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2)) == bg
                    dlX(agent(p).location(1),agent(p).location(2)) = bg;
                    agent(p).location = agent(p).location + move;
                    dlX(agent(p).location(1),agent(p).location(2)) = p+bg;
                    agent(p).experience{step,3} = 0;
                elseif dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2)) == rewardValue
                    % If the agent finds a reward:
                    % Move the agent
                    dlX(agent(p).location(1),agent(p).location(2)) = bg;
                    agent(p).location = agent(p).location + move;
                    dlX(agent(p).location(1),agent(p).location(2)) = p+bg;
                    
                    % Reward the agent
                    agent(p).experience{step,3} = 1;
                    
                    % Make new reward in the playing field
                    rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    while dlX(rewardLoc(1),rewardLoc(2)) ~= bg
                        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                    end
                    dlX(rewardLoc(1),rewardLoc(2)) = rewardValue;
                elseif dlX(agent(p).location(1) + move(1), agent(p).location(2) + move(2)) ~= bg
                    agent(p).experience{step,3} = 0;
                end
            else
                agent(p).experience{step,3} = -0.1;
            end
            % Save the changed playing field
            agent(p).dlX = dlX;
            agent(p).dlX(dlX == p+bg) = 0;
            agent(p).experience{step,4} = extractdata(agent(p).dlX);
            
            if stopCondition == true
                break;
            end
        end
        if stopCondition == true
            maxSteps = step;
            break;
        end
    end
    
    for p = 1:numAgents
        %% GENERALIZED ADVANTAGED ESTIMATOR
        [D, G] = GAE([agent(p).experience{:,3}],[agent(p).experience{:,6}],lambda,gamma);
        
        maxEpochs = 3;
        miniBatchSize = ceil(maxSteps/maxEpochs);
        eps = 0.2;
        
        for K = 1:maxEpochs
            % Shuffle the batch of experiences
            [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatch(miniBatchSize, agent(p).experience, G, D);
            
            exp_batch = dlarray(single(exp_batch),'SSCB');
            
            % Calculate the gradient from the batch of experiences
            [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,agent(p).actor,exp_batch,D_batch,r_old_batch,eps);
            [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,agent(p).critic,exp_batch,G_batch);
            
            % Update the network parameters using the SGDM optimizer.
            [agent(p).actor, agent(p).velocityActor] = sgdmupdate(agent(p).actor, gradientsActor, agent(p).velocityActor, learnRate, momentum);
            [agent(p).critic, agent(p).velocityCritic] = sgdmupdate(agent(p).critic, gradientsCritic, agent(p).velocityCritic, learnRate, momentum);
        end
        
        % Display the training progress.
        agent(p).rewards(i) = sum([agent(p).experience{:,3}]);
        averageReward = mean(agent(p).rewards(i-min(i-1,30):i));
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(agent(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            %             addpoints(agent(p).lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
            addpoints(agent(p).lineReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
        agent(p).experience = [];
    end
    
end
