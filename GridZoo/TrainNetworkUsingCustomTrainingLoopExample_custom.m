%% Train Network Using Custom Training Loop
% This example shows how to train a network that navigates a grid world.
% 
% If |trainingOptions| does not provide the options you need (for example, a 
% custom learning rate schedule), then you can define your own custom training 
% loop using automatic differentiation.
% 
% This example trains a network to classify handwritten digits with the _time-based 
% decay_ learning rate schedule: for each iteration, the solver uses the learning 
% rate given by $\rho_t =\frac{\rho_0 }{1+k\;t}$, where _t_ is the iteration number, 
% $\rho_0$ is the initial learning rate, and _k_ is the decay.
%% Load Training Data
% Initiate the grid world.

clear all
close all
gridWorld = ones(5,5);
actions = [1 2 3 4];
numActions = numel(actions);
% Define movement for each action
walking = [0 1; 0 -1; 1 0; -1 0];
maxSteps = 20;
%% Define Network
% Define the network and specify the average image using the |'Mean'| option 
% in the image input layer.

layers = [
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    convolution2dLayer(3, 16, 'Padding', 'same', 'Name', 'conv1')
    batchNormalizationLayer('Name','bn1')
    reluLayer('Name', 'relu1')
    convolution2dLayer(3, 16, 'Padding', 'same', 'Name', 'conv2')
    batchNormalizationLayer('Name','bn2')
    reluLayer('Name', 'relu2')
    fullyConnectedLayer(100, 'Name', 'fc1')
    reluLayer('Name', 'relu3')
    fullyConnectedLayer(100, 'Name', 'fc2')
    reluLayer('Name', 'relu4')
    fullyConnectedLayer(numActions, 'Name', 'fc3')];
lgraph = layerGraph(layers);
actor = dlnetwork(lgraph);
layers = [
    imageInputLayer([size(gridWorld) 1], 'Name', 'input', 'Normalization', 'none')
    convolution2dLayer(3, 16, 'Padding', 'same', 'Name', 'conv1')
    batchNormalizationLayer('Name','bn1')
    reluLayer('Name', 'relu1')
    convolution2dLayer(3, 16, 'Padding', 'same', 'Name', 'conv2')
    batchNormalizationLayer('Name','bn2')
    reluLayer('Name', 'relu2')
    fullyConnectedLayer(100, 'Name', 'fc1')
    reluLayer('Name', 'relu3')
    fullyConnectedLayer(100, 'Name', 'fc2')
    reluLayer('Name', 'relu4')
    fullyConnectedLayer(1, 'Name', 'fc3')];
lgraph = layerGraph(layers);
critic = dlnetwork(lgraph);

%% Define Model Gradients Function
% Create the function |modelGradients|, listed at the end of the example, that 
% takes a |dlnetwork| object |dlnet|, a mini-batch of input data |dlX| with corresponding 
% labels |Y| and returns the gradients of the loss with respect to the learnable 
% parameters in |dlnet| and the corresponding loss.
%% Specify Training Options
% Specify the training options.

velocityActor = [];
velocityCritic = [];
numEpochs = 20;
miniBatchSize = 128;
initialLearnRate = 0.0001;
momentum = 0.9;
decay = 0.001;
%% 
% Train on a GPU if one is available. Using a GPU requires Parallel Computing 
% Toolbox™ and a CUDA® enabled NVIDIA® GPU with compute capability 3.0 or higher.

executionEnvironment = "auto";
%% Train Model
% Train the model using a custom training loop.
% 
% For each epoch, shuffle the data and loop over mini-batches of data. At the 
% end of each epoch, display the training progress.
% 
% For each mini-batch:
%% 
% * Convert the labels to dummy variables.
% * Convert the data to |dlarray| objects with underlying type single and specify 
% the dimension labels |'SSCB'| (spatial, spatial, channel, batch).
% * For GPU training, convert to |gpuArray| objects.
% * Evaluate the model gradients and loss using |dlfeval| and the |modelGradients| 
% function.
% * Determine the learning rate for the time-based decay learning rate schedule.
% * Update the network parameters using the |sgdmupdate| function.
%% 
% Initialize the training progress plot.

plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    lineLossActor = animatedline('Color','red');
    lineLossCritic = animatedline('Color','blue');
    lineReward = animatedline('Color','green');
    xlabel("Iteration")
    ylabel("Loss")
elseif plots == "grid-plot"
    fh = imshow(gridWorld);
end
%% 
% Train the network.

iteration = 0;
start = tic;
maxEpisodes = 10000;
gamma = 0.95;
averageReward = 0;

% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    
    % Read mini-batch of data and convert the labels to dummy
    % variables.
    X = gridWorld;
    
    % Set the start locations of the agent and the reward
    agentLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
%     rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
    agentLoc = [1,1];
    rewardLoc = [5,5; 3,3; 5,3; 1,4];
    while all(agentLoc == rewardLoc)
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
    end
    
    % Color the matrix
    X(agentLoc(1),agentLoc(2)) = 2;
    for j = 1:size(rewardLoc,1)
    X(rewardLoc(j,1),rewardLoc(j,2)) = 3;
    end
    
    % Convert mini-batch of data to dlarray.
    dlX = dlarray(single(X),'SSCB');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    % experience is a cell array filled with:
    % State, Action, Reward and State+1
    experience = cell(20,4);
    
    for step = 1:maxSteps
        action = forward(actor,dlX);
        actionNr = randsample(numel(extractdata(action)), 1, true, extractdata(softmax(action)));
%         [~,actionNr] = max(extractdata(action));
        experience{step,1} = dlX;
        experience{step,2} = [0 0 0 0]';
        experience{step,2}(actionNr) = 1;
%         experience{step,2} = action;
        
        dlX(agentLoc(1),agentLoc(2)) = 1;
        
        newLoc = agentLoc + walking(actionNr,:);
        
        if all(newLoc > 0) && all(newLoc <= size(gridWorld))
            agentLoc = newLoc;
        end
        
        if dlX(agentLoc(1),agentLoc(2)) == 3
            reward = 10;
        else
            reward = 0;
        end
        
        dlX(agentLoc(1),agentLoc(2)) = 2;
                
        experience{step,3} = reward;
        experience{step,4} = dlX;
        %         imshow(extractdata(dlX))
    end
    % Evaluate the model gradients and loss using dlfeval and the
    % modelGradients function.
    %     [gradients,loss] = dlfeval(@modelGradients,actor,critic,dlX);
    for step = 1:maxSteps
        G(step) = dlarray(0);
        for k = step:maxSteps
            G(step) = G(step) + gamma^(k-step) .* experience{k,3};
        end
        if step ~= maxSteps
            G(step) = G(step) + gamma.^(maxSteps-step+1) .* ...
                forward(critic,experience{maxSteps,4});
        end
    end
    
    for step = 1:maxSteps
        D(step) = G(step) - forward(critic,experience{step,4});
    end
    
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    for step = 1:maxSteps
        exp_batch(:,:,1,step) = experience{step,1};
        D_batch(:,step) = D(step)*experience{step,2};
    end
    exp_batch = dlarray(single(exp_batch),'SSCB');
    [lossActor, gradientsActor] = dlfeval(@calculateGradientsActor,actor,exp_batch,D_batch);
    [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,critic,exp_batch,G);
    
    % Update the network parameters using the SGDM optimizer.
    [actor, velocityActor] = sgdmupdate(actor, gradientsActor, velocityActor, learnRate, momentum);
    [critic, velocityCritic] = sgdmupdate(critic, gradientsCritic, velocityCritic, learnRate, momentum);
    
    % Display the training progress.
    rewards(i) = sum([experience{:,3}]);
    averageReward = sum(rewards(i-min(i-1,30):i))/min(i,30);
    if plots == "training-progress"
        Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
        addpoints(lineLossActor,iteration,double(gather(extractdata(lossActor))))
        addpoints(lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
        addpoints(lineReward,iteration,averageReward)
        title("Elapsed: " + string(Dur))
        drawnow
    end
end

%% Model Gradients Function
% The |modelGradients| function takes a |dlnetwork| object |dlnet|, a mini-batch 
% of input data |dlX| with corresponding labels |Y| and returns the gradients 
% of the loss with respect to the learnable parameters in |dlnet| and the corresponding 
% loss. To compute the gradients automatically, use the |dlgradient| function.

function [l, gradients] = calculateGradientsActor(dlnet,dlX,D)
y = forward(dlnet,dlX);
l = crossentropy(softmax(y),D);
gradients = dlgradient(l,dlnet.Learnables);
end


function [l, gradients] = calculateGradientsCritic(dlnet,dlX,G)
y = forward(dlnet,dlX);
l = mse(y,G);
gradients = dlgradient(l,dlnet.Learnables);
end
%% 
% _Copyright 2019 The MathWorks, Inc._