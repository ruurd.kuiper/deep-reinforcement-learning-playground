%% Test Network Using Custom Training Loop

%% Load Test Data
% Initiate the grid world.
% clear all
% close all
load('agent.mat');

bg = 0;
gridWorld = ones(3,3).*bg;
% Define the possible actions
numAgents = 2;
numActions = 9;

%% Test on a GPU if one is available.
executionEnvironment = "cpu";

% Run a test
% Loop over episodes.
maxSteps = 5;

% Convert gridWorld to dlarray.
dlX = dlarray(single(gridWorld),'SSCB');

% If training on a GPU, then convert data to gpuArray.
if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
    dlX = gpuArray(dlX);
end

stopCondition = false;
for p = 1:numAgents
    agent(p).experience = [];
end

playerOrder = randperm(2);

figure; hold on
dlX(randi(3),randi(3),:,:) = playerOrder(2);

for step = 1:maxSteps
    % For each player shuffle the player order between matches
    
    for p = playerOrder
        imagesc(extractdata(dlX))
        pause(1)
        
        if p == 2
            temp = dlX;
            temp(dlX == 1) = 2;
            temp(dlX == 2) = 1;
            dlX = temp;
        end
        
        % Predict a move using the actor network
        action = softmax(extractdata(forward(agent(1).actor,dlX)));
        
        % Randomly select an action based on the output probabilities
        move = randsample(numActions, 1, true, action);
        
        % Change the playing field based on the action, only if the
        % action was legal (the player needs to learn the rules)
        [move(1),move(2)] = ind2sub(size(gridWorld),move);
        if dlX(move(1), move(2)) == bg
            dlX(move(1), move(2)) = 1;
        end
        
        % Check if the player has three in a row
        for j = 1:3
            X = extractdata(dlX);
            if all(X(:,j) == 1) || all(X(j,:) == 1) || ...
                    isequal(X(1,1),X(2,2),X(3,3),1) || ...
                    isequal(X(1,3),X(2,2),X(3,1),1)
                
                % Reward the player if he has won and break the loop
                stopCondition = true;
                break
            end
        end
        
        if p == 2
            temp = dlX;
            temp(dlX == 1) = 2;
            temp(dlX == 2) = 1;
            dlX = temp;
        end
        
        % End the game if there are no more legal moves
        if all(X ~= bg)
            stopCondition = true;
        end
        
        if stopCondition == true
            maxSteps = step;
            imagesc(extractdata(dlX))
            pause(1)
            break;
        end
    end
    
    
    
    if stopCondition == true
        maxSteps = step;
        break;
    end
    step
end
