%% Train multiple agents in a gridWorld with LSTMs
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld.
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it
% seems that the optimal solution might take longer if the network is to
% shallow.
% - Adding a (small) punishment value for illegal moves can also help
% - When the network is too big (too many nodes per layer) it will not
% converge

%% ZOO
% - In the zoo, all animals of the same species learn together

clear all
close all

%% Name:
datename = datestr(datetime('now'),30);

%% Initiate the gridWorld.
% Specify the gridWorld parameters
iterationsSinceLastUpdate = 0;
worldSize = [40 40];
numActions = 4;
numSpecies = 3;
numAnimals = 3;
% numRewards = ceil(worldSize(1)*worldSize(2)/5);
numWalls = 0;
numWallsUpdate = 1;

%% Set view to full world or smaller FOV
smallFOV = true;

% viewDistance = 3;
agentPixel = 2; % (+ the number of the species)
rewardPixel = 2;
wallPixel = 1;
rewardValue = 1;
standardReward = 0;
eatValue = 5;
punishValue = -1;
numChannels = 2 + numSpecies;

LOSrays = 4;
LOSrange = 20;

% Spawn settings
respawnReward = false;

% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.001;
momentum = 0.9;
decay = 0.001;
iteration = 0;
start = tic;
maxEpisodes = 10000;
gamma = 0.95;
lambda = 0.99;
eps = 0.2;
beta = 1;

averageReward = 0;
maxSteps = 128;
minSteps = 10;

maxEpochs = 1;%ceil(maxSteps/miniBatchSize);
miniBatchSize = round(maxSteps*numAnimals/1);

experienceReplay = true;

%% Network settings
executionEnvironment = "cpu";
pretrainedNetworks = false;
optimizer = 'adam';
lstmUnits = worldSize(1);%*worldSize(2);
fcUnits = worldSize(1);%*worldSize(2);
% if smallFOV == true
%     viewSize = [viewDistance*2+1 viewDistance*2+1 numChannels];
% else
%     viewSize = [worldSize numChannels];
% end

%% Choose between pretrained or new network
if pretrainedNetworks == true
    for p = 1:numSpecies
        pretrained = load(['species' num2str(p) '_20201231T152447.mat']);
        species(p).actor = pretrained.savedSpeciesToFile.actor;
        species(p).critic = pretrained.savedSpeciesToFile.critic;
        if strcmp(optimizer,'sgdm')
            species(p).velocityActor = pretrained.savedSpeciesToFile.velocityActor;
            species(p).velocityCritic = pretrained.savedSpeciesToFile.velocityCritic;
        elseif strcmp(optimizer,'adam')
            species(p).averageGradActor = pretrained.savedSpeciesToFile.averageGradActor;
            species(p).averageSqGradActor = pretrained.savedSpeciesToFile.averageSqGradActor;
            species(p).averageGradCritic = pretrained.savedSpeciesToFile.averageGradCritic;
            species(p).averageSqGradCritic = pretrained.savedSpeciesToFile.averageSqGradCritic;
        end
    end
else
    for p = 1:numSpecies
        sequenceInputSize = LOSrays*4*(3+numSpecies); % Input is the number of LOSrays/quarter view * 4 quarters * (number of view layers + distance layer)s
        sequenceInputSize = LOSrays*4*(4);
        layerSize = LOSrays*4*(3+1);
        layers = [
            sequenceInputLayer(sequenceInputSize, 'Name', 'input', 'Normalization', 'none')
            lstmLayer(layerSize, 'Name', 'lstm', 'OutputMode', 'last')
            leakyReluLayer('Name', 'relu1')
%             fullyConnectedLayer(layerSize, 'Name', 'fc1')
%             leakyReluLayer('Name', 'relu2')
            %             fullyConnectedLayer(sequenceInputSize/2, 'Name', 'fc2')
            %             reluLayer('Name', 'relu3')
            %             fullyConnectedLayer(sequenceInputSize/4, 'Name', 'fc3')
            %             reluLayer('Name', 'relu4')
            fullyConnectedLayer(numActions, 'Name', 'fcFinal')];
        lgraphActor = layerGraph(layers);
        
        layers = [
            sequenceInputLayer(sequenceInputSize, 'Name', 'input', 'Normalization', 'none')
            lstmLayer(layerSize, 'Name', 'lstm', 'OutputMode', 'last')
            leakyReluLayer('Name', 'relu1')
%             fullyConnectedLayer(layerSize, 'Name', 'fc1')
%             leakyReluLayer('Name', 'relu2')
            %             fullyConnectedLayer(sequenceInputSize/2, 'Name', 'fc2')
            %             reluLayer('Name', 'relu3')
            %             fullyConnectedLayer(sequenceInputSize/4, 'Name', 'fc3')
            %             reluLayer('Name', 'relu4')
            fullyConnectedLayer(1, 'Name', 'fcFinal')];
        lgraphCritic = layerGraph(layers);
        
        species(p).actor = dlnetwork(lgraphActor);
        species(p).critic = dlnetwork(lgraphCritic);
    end
end

for p = 1:numSpecies
    if strcmp(optimizer,'sgdm')
        species(p).velocityActor = [];
        species(p).velocityCritic = [];
    elseif strcmp(optimizer,'adam')
        species(p).averageGradActor = [];
        species(p).averageSqGradActor = [];
        species(p).averageGradCritic = [];
        species(p).averageSqGradCritic = [];
    end
    savedSpecies(1).actor(p) = species(p).actor;
    savedSpecies(1).critic(p) = species(p).critic;
end

%% Plot the training
plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numSpecies
        species(p).lineAverageReward = animatedline('Color','red');
        species(p).lineLossActor = animatedline('Color','blue');
        species(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end

borderThickness = 1;
%% Train the network
% Loop over episodes.
for i = 1:maxEpisodes
    numRewards = ceil(worldSize(1)*worldSize(2)/20);
    % Save the species for experience replay
    if i > 1
        if mod(i,10) == 0
            for p = 1:numSpecies
                savedSpecies(i/10+1).actor(p) = species(p).actor;
                savedSpecies(i/10+1).critic(p) = species(p).critic;
            end
        end
    end
    
    % Create the gridWorld
    gridWorld = zeros(worldSize(1),worldSize(2),numChannels);
    
    % Make the north and south pole
    gridWorld(1:borderThickness,:,wallPixel) = 1;
    gridWorld(end-borderThickness+1:end,:,wallPixel) = 1;
    gridWorld(:,1:borderThickness,wallPixel) = 1;
    gridWorld(:,end-borderThickness+1:end,wallPixel) = 1;
    
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    %     dlX = dlarray(single(gridWorld),'SSCB');
    dlX = gridWorld;
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    %% Switch up the order of players so there is no bias
    stopCondition = false;
    playOrderSpecies = randperm(numSpecies);
    playOrderAnimals = randperm(numAnimals);
    
    %% Give all players, rewards and walls a random spot on the map
    for p = 1:numSpecies
        for a = 1:numAnimals
            [dlX,species(p).animal(a).location] = newRandomLocation(dlX,agentPixel+p,a);
        end
    end
    
    for r = 1:numRewards
        [dlX,rewardLoc] = newRandomLocation(dlX,rewardPixel,1);
    end
    
    for r = 1:numWalls
        [dlX,wallLoc] = newRandomLocation(dlX,wallPixel,1);
    end
    
    for p = playOrderSpecies
        for a = playOrderAnimals
            species(p).animal(a).experience{1,1} = lineOfSight(dlX,species(p).animal(a).location,LOSrays,LOSrange);
            species(p).animal(a).experience{1,7} = dlarray(repmat(species(p).animal(a).experience{1,1},[1,1,minSteps]),'CBT');
        end
    end
    
    if experienceReplay && numSpecies > 1
        for p = 1:numSpecies
            %% This is for experience replay
            useSaved = zeros(p,1);
            if i>1 && all(useSaved == 0)
                useSaved(p) = randsample([0 1],1,true,[0.7 0.3]);
            else
                useSaved(p) = 0;
            end
            if useSaved(p) == false
                currentActor(p) = species(p).actor;
                currentCritic(p) = species(p).critic;
            else
                randomSavedSpeciesNr = randi(length(savedSpecies));
                currentActor(p) = savedSpecies(randomSavedSpeciesNr).actor(p);
                currentCritic(p) = savedSpecies(randomSavedSpeciesNr).critic(p);
            end
        end
    else
        for p = 1:numSpecies
            useSaved = zeros(p,1);
            currentActor(p) = species(p).actor;
            currentCritic(p) = species(p).critic;
        end
    end
    
    for step = 1:maxSteps
        %% This is to at least always give some reward
        for p = playOrderSpecies
            for a = playOrderAnimals
                species(p).animal(a).experience{step,3} = standardReward;
            end
        end
        for p = playOrderSpecies
            for a = playOrderAnimals
                
                if step > 1
                    %% Make a small FOV representation of the gridWorld for the player
                    species(p).animal(a).experience{step,1} = lineOfSight(dlX,species(p).animal(a).location,LOSrays,LOSrange);
                    
                    % Add the information of this step to the sequence input
                    species(p).animal(a).experience{step,7} = cat(3,species(p).animal(a).experience{step-1,7},species(p).animal(a).experience{step,1});
                    
                    % Put the information in a dlarray
                    %                     if step < minSteps
                    %                         species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7},'CBT');
                    %                     else
                    species(p).animal(a).experience{step,7} = dlarray(species(p).animal(a).experience{step,7}(:,:,end-minSteps+1:end),'CBT');
                    %                     end
                end
            end
            
            for a = playOrderAnimals
                while true
                    % Try catch in a loop, to load a past working
                    % actor/critic if the current one has broken
                    try
                        % Predict a move using the actor network
                        pred = extractdata(gather(forward(currentActor(p),species(p).animal(a).experience{step,7})));
                        
                        % Softmax the prediction
                        action = softmax(pred);
                        
                        % Randomly select an action based on the output probabilities
                        move = randsample(numActions, 1, true, action);
                        break
                    catch
                        pause(3)
                        pretrained = load(['species' num2str(p) '_' datename '.mat']);
                        pause(3)
                        species(p).actor = pretrained.savedSpeciesToFile.actor;
                        species(p).critic = pretrained.savedSpeciesToFile.critic;
                        if strcmp(optimizer,'sgdm')
                            species(p).velocityActor = pretrained.savedSpeciesToFile.velocityActor;
                            species(p).velocityCritic = pretrained.savedSpeciesToFile.velocityCritic;
                        elseif strcmp(optimizer,'adam')
                            species(p).averageGradActor = pretrained.savedSpeciesToFile.averageGradActor;
                            species(p).averageSqGradActor = pretrained.savedSpeciesToFile.averageSqGradActor;
                            species(p).averageGradCritic = pretrained.savedSpeciesToFile.averageGradCritic;
                            species(p).averageSqGradCritic = pretrained.savedSpeciesToFile.averageSqGradCritic;
                        end
                        currentActor(p) = species(p).actor;
                        currentCritic(p) = species(p).critic;
                    end
                end
                
                % Save the current state (1), the action taken (2), the softmax of
                % the action (5) and the prediction of the critic (6)
                species(p).animal(a).experience{step,2} = zeros(numActions,1);
                species(p).animal(a).experience{step,2}(move) = 1;
                species(p).animal(a).experience{step,5} = action;
                if step < minSteps
                    species(p).animal(a).experience{step,6} = extractdata(gather(forward(currentCritic(p),species(p).animal(a).experience{step,7})));
                else
                    species(p).animal(a).experience{step,6} = extractdata(gather(forward(currentCritic(p),species(p).animal(a).experience{step,7}(:,:,end-minSteps+1:end))));
                end
                
                
                oldLoc = species(p).animal(a).location;
                
                if move ~= 5
                    %% Change the playing field based on the action
                    [move(1),move(2)] = deal(actions(move,1),actions(move,2));
                    
                    newLoc = species(p).animal(a).location + move;
                    
                    if newLoc(1)<1
                        newLoc(1) = size(dlX,1) + newLoc(1);
                    elseif newLoc(1)>size(dlX,1)
                        newLoc(1) = newLoc(1) - size(dlX,1);
                    end
                    if newLoc(2)<1
                        newLoc(2) = size(dlX,2) + newLoc(2);
                    elseif newLoc(2)>size(dlX,2)
                        newLoc(2) = newLoc(2) - size(dlX,2);
                    end
                    
                    
                    %% Check if the movement is to an empty spot
                    if all(dlX(newLoc(1), newLoc(2),:) == 0)
                        % Set previous location to zero
                        dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        % Set agent location to new location
                        species(p).animal(a).location = newLoc;
                        % Set new pixel to species pixel value
                        dlX(newLoc(1),newLoc(2),agentPixel+p) = a;
                        % Give zero reward
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + 0;
                        
                        %% If the movement is to a reward spot
                    elseif dlX(newLoc(1), newLoc(2),rewardPixel) == 1
                        % Set previous location to zero
                        dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        % Set agent location to new location
                        species(p).animal(a).location = newLoc;
                        % Set reward pixel to zero
                        dlX(newLoc(1),newLoc(2),rewardPixel) = 0;
                        % Reward the animal
%                         species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + (1 + numRewards-sum(sum(dlX(:,:,rewardPixel))))*rewardValue;
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + rewardValue;
                        % Make new reward in the world at random free location
                        if respawnReward == true
                            [dlX,rewardLoc] = newRandomLocation(dlX,rewardPixel,1);
                        end
                        
                        % If all rewards have been found, reward the
                        % species
                        if sum(sum(dlX(:,:,rewardPixel))) == 0
                            for ii = 1:numAnimals
                                %                                 species(p).animal(ii).experience{step,3} = species(p).animal(ii).experience{step,3} + numRewards;
                            end
                        end
                        
                        % Set new pixel to species pixel value
                        dlX(newLoc(1),newLoc(2),agentPixel+p) = a;
                        %% If the move is to another species pixel
                    elseif any(dlX(newLoc(1), newLoc(2),agentPixel+1:end) ~= 0)
                        % Do not move, punish the player
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                        
                        %                         if p == 1
                        %                             % Set previous location to zero
                        %                             dlX(oldLoc(1),oldLoc(2),agentPixel+p) = 0;
                        %                             % Set animal location to new location
                        %                             species(p).animal(a).location = newLoc;
                        %                             % Set new pixel to species pixel value
                        %                             dlX(newLoc(1),newLoc(2),agentPixel+p) = a;
                        %
                        %                             % Find which animal is being eaten
                        %                             eatenAnimal = dlX(newLoc(1), newLoc(2),4);
                        %                             dlX(newLoc(1), newLoc(2),4) = 0;
                        %                             [dlX,species(2).animal(eatenAnimal).location] = newRandomLocation(dlX,4,eatenAnimal);
                        %
                        %                             % Reward the eating animal
                        %                             species(p).animal(a).experience{step,3} = species(1).animal(1).experience{step,3} + eatValue;
                        %                             % Punish the eaten animal
                        %                             species(2).animal(eatenAnimal).experience{step,3} = species(2).animal(eatenAnimal).experience{step,3} - eatValue;
                        %                         end
                        %% If the move is to a same species pixel
                    elseif dlX(newLoc(1), newLoc(2),agentPixel+p) ~= 0
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                        %% If the move is to a wall pixel
                    elseif dlX(newLoc(1), newLoc(2),wallPixel) ~= 0
                        % Do not move, punish the player
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                        % If the movement is to anything else (such as another
                        % player)
                    elseif ~all(dlX(newLoc(1), newLoc(2),:) == 0)
                        % Do not move, give zero reward
                        species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + 0;
                    end
                elseif move == 5
                    %                     if dlX(oldLoc(1), oldLoc(2),wallPixel) == 0 && ...
                    %                             dlX(oldLoc(1), oldLoc(2),rewardPixel) == 0
                    %                         dlX(oldLoc(1), oldLoc(2),wallPixel) = 1;
                    %                         species(p).animal(a).experience{step,3} = 0;
                    %                     else
                    species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} + punishValue;
                    %                     end
                end
                
                species(p).animal(a).experience{step,4} = dlX;
                
                if a == 1
                    zooWorld{step} = dlX;
                end
            end
        end
        
        % Update these each step
        newMaxSteps = step*numAnimals;
        newMinSteps = min(minSteps,step);
        newMiniBatchSize = round(newMaxSteps);
        
        % If all rewards have been found, end this episode
        if sum(sum(dlX(:,:,rewardPixel))) == 0
            break
        end
        
    end
    
    for p = playOrderSpecies
        species(p).experience = {};
        for a = playOrderAnimals
            species(p).experience = cat(1,species(p).experience,species(p).animal(a).experience);
        end
    end
    
    for p = playOrderSpecies
        if useSaved(p) == false
            %% GENERALIZED ADVANTAGED ESTIMATOR
            %% Calculate advantage
            [D, G] = GAE([species(p).experience{:,3}],[species(p).experience{:,6}],lambda,gamma);
            for K = 1:maxEpochs
                % Shuffle the batch of experiences
                [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatchTimeSeriesLOS(newMiniBatchSize, species(p).experience, G, D, newMinSteps);
                
                exp_batch = dlarray(exp_batch,'CBT');
                
                % Calculate the gradient from the batch of experiences
                [lossActor, gradientsActor] = dlfeval(@calculateGradientsActorPPO,species(p).actor,exp_batch,D_batch,r_old_batch,eps,beta);
                [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,species(p).critic,exp_batch,G_batch);
                
                if strcmp(optimizer,'sgdm')
                    % Update the network parameters using the SGDM optimizer.
                    [species(p).actor, species(p).velocityActor] = sgdmupdate(species(p).actor, gradientsActor, species(p).velocityActor, learnRate, momentum);
                    [species(p).critic, species(p).velocityCritic] = sgdmupdate(species(p).critic, gradientsCritic, species(p).velocityCritic, learnRate, momentum);
                elseif strcmp(optimizer,'adam')
                    % Update the network parameters using the ADAM optimizer.
                    [species(p).actor,species(p).averageGradActor,species(p).averageSqGradActor] = adamupdate(species(p).actor,gradientsActor,species(p).averageGradActor,species(p).averageSqGradActor,iteration,learnRate);
                    [species(p).critic,species(p).averageGradCritic,species(p).averageSqGradCritic] = adamupdate(species(p).critic,gradientsCritic,species(p).averageGradCritic,species(p).averageSqGradCritic,iteration,learnRate);
                end
            end
        end
        
        %% Display the training progress.
        species(p).rewards(i) = 0;
        if useSaved(p) && i > 1
            species(p).rewards(i) = species(p).rewards(i-1);
        else
            for a = playOrderAnimals
                species(p).rewards(i) = species(p).rewards(i) + sum([species(p).animal(a).experience{:,3}]);
            end
        end
        
        species(p).averageReward(i) = mean(species(p).rewards(i-min(i-1,30):i));
        if iterationsSinceLastUpdate > 500
            iterationsSinceLastUpdate = 0;
            if species(p).averageReward(i) >= 0.8*numRewards%0.1*maxSteps*rewardValue
                %                         numWalls = numWalls+numWallsUpdate;
                %                 worldSize = worldSize+1;
                iterationsSinceLastUpdate = 0;
            end
        end
        iterationsSinceLastUpdate = iterationsSinceLastUpdate + 1;
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(species(p).lineReward,iteration,species(p).rewards(end))
            addpoints(species(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            addpoints(species(p).lineAverageReward,iteration,species(p).averageReward(i))
            title("Elapsed: " + string(Dur))
            drawnow
        end
        if all(species(p).averageReward(1:end-1) <=  species(p).averageReward(end))
            savedSpeciesToFile = species(p);
            save(['species' num2str(p) '_' datename '.mat'],'savedSpeciesToFile');
            save(['zooWorld_' datename '.mat'],'zooWorld');
        end
        
    end
    
    %% Reset all experiences for the next episode
    for p = playOrderSpecies
        for a = playOrderAnimals
            species(p).animal(a).experience = {};
        end
    end
    save(['zooWorld_' datename '_Final.mat'],'zooWorld');
    zooWorld = [];
end


