%% Draw on sphere!
clear all
close all

figure;
fps = 25;
colormap(lines)

vis = 'grid';
while true
    switch vis
        case 'grid'
            %% Show as grid
            try
                load('zooWorld3.mat');
            catch
                pause(0.5);
                load('zooWorld3.mat');
            end
            hold on
            for i = 1:length(zooWorld)
                z = extractdata(zooWorld{i});
%                 I(M == 0) = 0;
                %                 imshow(imresize(mat2gray(I,[0 size(z,3)]),50,'nearest'));
                imshow(imresize(ind2rgb(z+1,lines),50,'nearest'));
                drawnow
                pause(0.05)
            end
            cla
        case 'planet'
            %% Show as planet
            load('zooWorld.mat');
            [X,Y,Z] = sphere(size(zooWorld{1},1));
            zooPlanet = surfl(X,Y,Z,'light');
            zooPlanet(1).CData = zeros([size(zooPlanet(1).ZData)]);
            zooPlanet(1).EdgeColor = 'none';
            lighting gouraud
            axis off
            axis('vis3d')
            set(gcf,'Color','k')
            for i = 1:length(zooWorld)
                %         [~, zooPlanet.CData] = max(extractdata(zooWorld{i}),[],3);
                zooPlanet(1).CData = extractdata(zooWorld{i})+0.2;
                drawnow
                for j = 1:fps
                    a = 360/length(zooWorld)*(i+j/fps);
                    view([a sin((90+a)*pi/180)*90/pi]);
                    pause(0.002)
                end
            end
            cla
    end
end