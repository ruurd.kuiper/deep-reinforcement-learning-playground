%% Train multiple agents in a gridWorld with LSTMs
% Multiple agents consisting of a actor and a critic network are trained to
% find rewards in a gridWorld.
%% RESEARCH NOTES
% - PPO with GAE seems to work better than vanilla actor-critic network
% - ADAM optimizer seems to work better than SGDM
% - More layers in the network does make it slower to compute
% - More layers in the network (depending on the complexity of the task)
% might make initial training slower. However, even for simple tasks, it
% seems that the optimal solution might take longer if the network is to
% shallow.
% - Adding a (small) punishment value for illegal moves can also help
% - When the network is too broad (too many nodes per layer) it will not
% converge
% - If only fully connected layers are used, it is easier to use 1D input

%% ZOO
% - In the zoo, all animals of the same species learn together

clear all
close all
%% Initiate the gridWorld.
% Specify the gridWorld parameters
iterationsSinceLastUpdate = 0;

numActions = 4;
numSpecies = 1;
numAnimals = 2;
numRewards = 0;
numWalls = 0;
numWallsUpdate = 1;

viewDistance = 3;
agentPixel = 1;
rewardPixel = 2;
wallPixel = 3;
rewardValue = 1;
punishValue = -1;
numChannels = 3;
worldSize = [6 6];
gridWorld = zeros(worldSize(1),worldSize(2),numChannels);

% Spawn settings
respawnReward = true;

% Make the north and south pole
gridWorld(1:viewDistance,:,wallPixel) = 1;
gridWorld(end-viewDistance+1:end,:,wallPixel) = 1;
gridWorld(:,end-viewDistance+1:end,wallPixel) = 1;
gridWorld(:,1:viewDistance,wallPixel) = 1;

% Define the possible actions
actions = [1 0; -1 0; 0 1; 0 -1];

%% Specify Training Options
% Specify the training options.
initialLearnRate = 0.0001;
momentum = 0.9;
decay = 0.01;
iteration = 0;
start = tic;
maxEpisodes = 100000;
gamma = 0.98;
lambda = 0.98;
eps = 0.1;
beta = 0.1;

averageReward = 0;
maxSteps = 45;
minSteps = 5;

miniBatchSize = 20;
maxEpochs = 2;%ceil(maxSteps/miniBatchSize);

%% Define Network
% Define the network and specify the average image using the |'Mean'| option
% in the image input layer.

% Network settings
executionEnvironment = "cpu";
pretrainedNetworks = false;
optimizer = 'adam';
viewSize = viewDistance*2+1;
lstmUnits = viewSize^2;
fcUnits = viewSize^2;

if pretrainedNetworks == true
    pretrained = load('zooWorld_1_Agent.mat');
    for p = 1:numSpecies
        species(p).actor = pretrained.species.actor;
        species(p).critic = pretrained.species.critic;
    end
else
    for p = 1:numSpecies
        
        layers = [
            sequenceInputLayer(numel(gridWorld)+numActions, 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            lstmLayer(lstmUnits, 'Name', 'lstm', 'OutputMode', 'last')
            fullyConnectedLayer(fcUnits, 'Name', 'fc1')
            reluLayer('Name', 'relu1')
            fullyConnectedLayer(fcUnits, 'Name', 'fc2')
            reluLayer('Name', 'relu2')
            %             fullyConnectedLayer(fcUnits, 'Name', 'fc3')
            %             reluLayer('Name', 'relu3')
            %             fullyConnectedLayer(numActions + viewSize^2, 'Name', 'fcFinal')];
            fullyConnectedLayer(numActions + numActions, 'Name', 'fcFinal')];
        lgraphActor = layerGraph(layers);
        
        layers = [
            sequenceInputLayer(numel(gridWorld)+numActions, 'Name', 'input', 'Normalization', 'zerocenter','Mean',0.5)
            lstmLayer(lstmUnits, 'Name', 'lstm', 'OutputMode', 'last')
            fullyConnectedLayer(fcUnits, 'Name', 'fc1')
            reluLayer('Name', 'relu1')
            fullyConnectedLayer(fcUnits, 'Name', 'fc2')
            reluLayer('Name', 'relu2')
%             fullyConnectedLayer(25, 'Name', 'fc3')
%             reluLayer('Name', 'relu3')
            fullyConnectedLayer(1, 'Name', 'fcFinal')];
        lgraphCritic = layerGraph(layers);
        
        species(p).actor = dlnetwork(lgraphActor);
        species(p).critic = dlnetwork(lgraphCritic);
    end
end

for p = 1:numSpecies
    if strcmp(optimizer,'sgdm')
        species(p).velocityActor = [];
        species(p).velocityCritic = [];
    elseif strcmp(optimizer,'adam')
        species(p).averageGradActor = [];
        species(p).averageSqGradActor = [];
        species(p).averageGradCritic = [];
        species(p).averageSqGradCritic = [];
    end
end

%% Plot the training
plots = "training-progress";
if plots == "training-progress"
    figure
    hold on
    for p = 1:numSpecies
        species(p).lineLossActor = animatedline('Color','red');
        species(p).lineLossCritic = animatedline('Color','blue');
        species(p).lineReward = animatedline('Color','green');
        xlabel("Iteration")
        ylabel("Loss")
    end
end

%% Train the network
% Loop over episodes.
for i = 1:maxEpisodes
    iteration = iteration + 1;
    counter = 0;
    % Determine learning rate for time-based decay learning rate schedule.
    learnRate = initialLearnRate/(1 + decay*iteration);
    
    % Convert gridWorld to dlarray.
    dlX = dlarray(single(gridWorld),'CBT');
    
    % If training on a GPU, then convert data to gpuArray.
    if (executionEnvironment == "auto" && canUseGPU) || executionEnvironment == "gpu"
        dlX = gpuArray(dlX);
    end
    
    stopCondition = false;
    % For each player shuffle the player order between matches
    playOrderSpecies = randperm(numSpecies);
    playOrderAnimals = randperm(numAnimals);
    
    %% This is for initially placing the agents, rewards and random walls
    for p = 1:numSpecies
        for a = 1:numAnimals
            species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            while any(dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),:) ~= 0)
                species(p).animal(a).location = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
            end
            dlX(species(p).animal(a).location(1),species(p).animal(a).location(2),agentPixel) = 1;
        end
    end
    
    for r = 1:numRewards
        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
    end
    
    for r = 1:numWalls
        wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        while any(dlX(wallLoc(1),wallLoc(2),:) == 1)
            wallLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
        end
        dlX(wallLoc(1),wallLoc(2),wallPixel) = 1;
    end
    
    % Make a separate representation of the gridWorld for the
    % player
    for p = playOrderSpecies
        for a = playOrderAnimals
            %% This is used if the player only sees a limited FOV
%             bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
%             bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
%             
%             if any(bb1<1)
%                 bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
%             end
%             if any(bb1>size(dlX,1))
%                 bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
%             end
%             if any(bb2<1)
%                 bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
%             end
%             if any(bb2>size(dlX,2))
%                 bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
%             end
%             
%             % Save the field of view of this step in the experience
%             tmp = dlX(bb1,bb2,:);
%             species(p).animal(a).experience{1,1} = tmp(:);
            %% This is used if the player sees the full world
            species(p).animal(a).experience{1,1} = dlX(:);
            % Add zeros for the communication of other animals of the same species
            for aa = 1:length(species(p).animal)
                if aa ~= a
                    species(p).animal(a).experience{1,1} = [species(p).animal(a).experience{1,1} zeros(1,numActions)];
                end
            end
            
            species(p).animal(a).experience{1,8} = zeros(viewSize,viewSize);
            species(p).animal(a).experience{1,7} = dlarray(species(p).animal(a).experience{1,1},'CBT');
        end
    end
    
    for step = 1:maxSteps
        % Save the world so we can watch it later
        zooWorld{step} = dlX;
        
        for p = playOrderSpecies
            for a = playOrderAnimals
                
                if step > 1
                    
                    % Find the Field Of View of the animal
%                     bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
%                     bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
%                     
%                     % If the field of view is outside the world, wrap around
%                     if any(bb1<1)
%                         bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
%                     end
%                     if any(bb1>size(dlX,1))
%                         bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
%                     end
%                     if any(bb2<1)
%                         bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
%                     end
%                     if any(bb2>size(dlX,2))
%                         bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
%                     end
                    
                    % Add all the communication of other animals of the same species
                    for aa = 1:length(species(p).animal)
                        if aa ~= a
                            % Save the field of view of this step in the experience
                            species(p).animal(a).experience{1,1} = dlX(:);

                            % Concatenate the field of view with the communication
                            species(p).animal(a).experience{1,1} = [species(p).animal(a).experience{1,1}(1) species(p).animal(aa).experience{1,8}];
                            
                            % Add the information of this step to the sequence input
                            species(p).animal(aa).experience{step,7} = cat(5,species(p).animal(aa).experience{step-1,7},species(p).animal(aa).experience{step,1});
                            
                            % Put the information in a dlarray
                            species(p).animal(aa).experience{step,7} = dlarray(species(p).animal(aa).experience{step,7},'CBT');
                        end
                    end             
                end
            end
            
            for a = playOrderAnimals
                % Predict a move using the actor network
                if step < minSteps
                    pred = extractdata(gather(forward(species(p).actor,species(p).animal(a).experience{step,7})));
                else
                    pred = extractdata(gather(forward(species(p).actor,species(p).animal(a).experience{step,7}(:,:,:,:,end-minSteps+1:end))));
                end
                
                % Separate the prediction into action and communication
                action = softmax(pred(1:4));
                advice = softmax(pred(5:8));
                
                % Randomly select an action based on the output probabilities
                move = randsample(numActions, 1, true, action);
                
                % Save the current state (1), the action taken (2), the softmax of
                % the action (5) and the prediction of the critic (6)
                species(p).animal(a).experience{step,2} = zeros(numActions,1);
                species(p).animal(a).experience{step,2}(move) = 1;
                species(p).animal(a).experience{step,5} = action;
                species(p).animal(a).experience{step,6} = extractdata(gather(forward(species(p).critic,species(p).animal(a).experience{step,7})));
                species(p).animal(a).experience{step,8}
                %% Change the playing field based on the action
                [move(1),move(2)] = deal(actions(move,1),actions(move,2));
                
                oldLoc = species(p).animal(a).location;
                newLoc = species(p).animal(a).location + move;
                
                if newLoc(1)<1
                    newLoc(1) = size(dlX,1) + newLoc(1);
                elseif newLoc(1)>size(dlX,1)
                    newLoc(1) = newLoc(1) - size(dlX,1);
                end
                if newLoc(2)<1
                    newLoc(2) = size(dlX,2) + newLoc(2);
                elseif newLoc(2)>size(dlX,2)
                    newLoc(2) = newLoc(2) - size(dlX,2);
                end
                
                
                %% Check if the movement is to an empty spot
                if all(dlX(newLoc(1), newLoc(2),:) == 0)
                    % Set previous location to zero
                    dlX(oldLoc(1),oldLoc(2),agentPixel) = 0;
                    % Set agent location to new location
                    species(p).animal(a).location = newLoc;
                    % Set new pixel to species pixel value
                    dlX(newLoc(1),newLoc(2),agentPixel) = p/numSpecies;
                    % Give zero reward
                    species(p).animal(a).experience{step,3} = 0;
                    
                    %% If the movement is to a reward spot
                elseif dlX(newLoc(1), newLoc(2),rewardPixel) == 1
                    % Set previous location to zero
                    dlX(oldLoc(1),oldLoc(2),agentPixel) = 0;
                    % Set agent location to new location
                    species(p).animal(a).location = newLoc;
                    % Set reward pixel to zero
                    dlX(newLoc(1),newLoc(2),rewardPixel) = 0;
                    % Set new pixel to species pixel value
                    dlX(newLoc(1),newLoc(2),agentPixel) = p/numSpecies;
                    % Reward the animal
                    species(p).animal(a).experience{step,3} = rewardValue;
                    
                    if respawnReward == true
                        % Make new reward in the playing field at random
                        % location that is not already occupied
                        rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                        while any(dlX(rewardLoc(1),rewardLoc(2),:) == 1)
                            rewardLoc = [randi(size(gridWorld,1)),randi(size(gridWorld,2))];
                        end
                        dlX(rewardLoc(1),rewardLoc(2),rewardPixel) = 1;
                    end
                    
                    %% If the move is to a wall pixel
                elseif dlX(newLoc(1), newLoc(2),wallPixel) ~= 0
                    % Do not move, punish the player
%                     species(p).animal(a).experience{step,3} = punishValue;
                    for aa = 1:length(species(p).animal)
                        species(p).animal(aa).experience{step,3} = punishValue;
                    end
                    % If the movement is to anything else (such as another
                    % player)
                    
                    %% If the move is to another player pixel
                elseif dlX(newLoc(1), newLoc(2),agentPixel) ~= 0
                    % Set previous location to zero
                    dlX(oldLoc(1),oldLoc(2),agentPixel) = 0;
                    % Set agent location to new location
                    species(p).animal(a).location = newLoc;
                    % Set new pixel to species pixel value
                    dlX(newLoc(1),newLoc(2),agentPixel) = p/numSpecies;
                    % Reward all animals of that species
%                     for aa = 1:length(species(p).animal)
%                         species(p).animal(aa).experience{step,3} = rewardValue;
%                     end
                    
                elseif ~all(dlX(newLoc(1), newLoc(2),:) == 0)
                    % Do not move, give zero reward
                    species(p).animal(a).experience{step,3} = 0;
                end
                
                % Save the changed playing field
                bb1 = [species(p).animal(a).location(1)-viewDistance:species(p).animal(a).location(1)+viewDistance];
                bb2 = [species(p).animal(a).location(2)-viewDistance:species(p).animal(a).location(2)+viewDistance];
                
                if any(bb1<1)
                    bb1(bb1 < 1) = size(dlX,1) + bb1(bb1<1);
                end
                if any(bb1>size(dlX,1))
                    bb1(bb1 > size(dlX,1)) = bb1(bb1 > size(dlX,1)) - size(dlX,1);
                end
                if any(bb2<1)
                    bb2(bb2 < 1) = size(dlX,2) + bb1(bb2<1);
                end
                if any(bb2>size(dlX,2))
                    bb2(bb2 > size(dlX,2)) = bb2(bb2 > size(dlX,2)) - size(dlX,2);
                end
                
                species(p).animal(a).experience{step,4} = extractdata(dlX(bb1,bb2,:));
            end
            
            %% This is a reward for animals that perform the same action
%             if step > 1
%                 for a = 2:length(species(p).animal)
%                     if all(species(p).animal(a-1).experience{step,2} == ...
%                             species(p).animal(a).experience{step,2}) && ...
%                         ~all(species(p).animal(a-1).experience{step-1,2} == ...
%                             species(p).animal(a-1).experience{step,2})
%                         species(p).animal(a-1).experience{step,3} = species(p).animal(a-1).experience{step,3} + 1;
%                         species(p).animal(a).experience{step,3} = species(p).animal(a).experience{step,3} +1 ;
%                     end
%                 end
%             end
        end
    end
    
    
    
    for p = playOrderSpecies
        %% GENERALIZED ADVANTAGED ESTIMATOR
        for a = playOrderAnimals
            [D, G] = GAE([species(p).animal(a).experience{:,3}],[species(p).animal(a).experience{:,6}],lambda,gamma);
            
            for K = 1:maxEpochs
                % Shuffle the batch of experiences
                [exp_batch, G_batch, r_old_batch, D_batch] = shuffleBatchTimeSeries(miniBatchSize, species(p).animal(a).experience, G, D, minSteps);
                
                exp_batch = dlarray(exp_batch,'CBT');
                
                % Calculate the gradient from the batch of experiences
                [lossActor, gradientsActor] = dlfeval(@calculateGradientsActor,species(p).actor,exp_batch,D_batch,eps,beta);
                [lossCritic, gradientsCritic] = dlfeval(@calculateGradientsCritic,species(p).critic,exp_batch,G_batch);
                
                if strcmp(optimizer,'sgdm')
                    % Update the network parameters using the SGDM optimizer.
                    [species(p).actor, species(p).velocityActor] = sgdmupdate(species(p).actor, gradientsActor, species(p).velocityActor, learnRate, momentum);
                    [species(p).critic, species(p).velocityCritic] = sgdmupdate(species(p).critic, gradientsCritic, species(p).velocityCritic, learnRate, momentum);
                elseif strcmp(optimizer,'adam')
                    % Update the network parameters using the ADAM optimizer.
                    [species(p).actor,species(p).averageGradActor,species(p).averageSqGradActor] = adamupdate(species(p).actor,gradientsActor,species(p).averageGradActor,species(p).averageSqGradActor,iteration);
                    [species(p).critic,species(p).averageGradCritic,species(p).averageSqGradCritic] = adamupdate(species(p).critic,gradientsCritic,species(p).averageGradCritic,species(p).averageSqGradCritic,iteration);
                end
            end
        end
        
        %% Display the training progress.
        species(p).rewards(i) = sum([species(p).animal(a).experience{:,3}]);
        averageReward = mean(species(p).rewards(i-min(i-1,30):i));
%         if iterationsSinceLastUpdate > 5
%             if averageReward >= 10
%                 numWalls = numWalls+numWallsUpdate;
%                 iterationsSinceLastUpdate = 0;
%             end
%         end
        iterationsSinceLastUpdate = iterationsSinceLastUpdate + 1;
        if plots == "training-progress"
            Dur = duration(0,0,toc(start),'Format','hh:mm:ss');
            addpoints(species(p).lineLossActor,iteration,double(gather(extractdata(lossActor))))
            %             addpoints(species(p).lineLossCritic,iteration,double(gather(extractdata(lossCritic))))
            addpoints(species(p).lineReward,iteration,averageReward)
            title("Elapsed: " + string(Dur))
            drawnow
        end
        species(p).animal(a).experience = [];
    end
    
    save('zooWorld.mat','zooWorld');
end
