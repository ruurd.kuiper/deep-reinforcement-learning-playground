function in = resetMap(in,obsMat)
% Reset function for multi agent area coverage example

% Copyright 2020 The MathWorks, Inc.

gridSize = [12 12];

%% Reset position to a random location
isvalid = false;
while ~isvalid
    rows = randperm(gridSize(1),3);
    cols = randperm(gridSize(2),3);
    s0 = [rows' cols'];
    if all(~all(s0(1,:) == obsMat,2)) && all(~all(s0(2,:) == obsMat,2)) && all(~all(s0(3,:) == obsMat,2))
        isvalid = true;
    end
end

%% Reset positions always to the same starting position
% sA0 = [2 2];
% sB0 = [11 4];
% sC0 = [3 12];
% s0 = [sA0; sB0; sC0];


in = setVariable(in, 's0', s0);
end