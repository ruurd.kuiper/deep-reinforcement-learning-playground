function in = resetMap2(in,obsMat,nr)
% Reset function for multi agent area coverage example

% Copyright 2020 The MathWorks, Inc.

gridSize = [12 12];

%% Reset position to a random location
for i = 1:nr
    row = randi([1, gridSize(1)]);
    col = randi([1, gridSize(2)]);
    while ismember([row col],s0(i,:),'rows')
        row = randi([1, gridSize(1)]);
        col = randi([1, gridSize(2)]);
    end
    s0(i,:) = [row col];
end

%% Reset positions always to the same starting position
% sA0 = [2 2];
% sB0 = [11 4];
% sC0 = [3 12];
% s0 = [sA0; sB0; sC0];


in = setVariable(in, 's0', s0);
end