function [sight,sightLine] = lineOfSightFast(matrix,location,rays,viewDistance)
% Create the lines
i = 1;
stepsize = 10;
%% First find the coordinates of the pixels in each straight sight line
for x = -viewDistance:viewDistance*2:viewDistance
    for y = -viewDistance:viewDistance*2/rays:viewDistance
        if ~(x==0 && y==0)
            for l = 1:stepsize:viewDistance
                sightLine{i}(l,1) = round(l/viewDistance*x) + location(1);
                sightLine{i}(l,2) = round(l/viewDistance*y) + location(2);
            end
            i = i+1;
        end
    end
end

%% First find the coordinates of the pixels in each sight line
for x = -viewDistance+viewDistance*2/rays:viewDistance*2/rays:viewDistance-viewDistance*2/rays
    for y = -viewDistance:viewDistance*2:viewDistance
        if ~(x==0 && y==0)
            for l = 1:stepsize:viewDistance
                sightLine{i}(l,1) = round(l/viewDistance*x) + location(1);
                sightLine{i}(l,2) = round(l/viewDistance*y) + location(2);
            end
            i = i+1;
        end
    end
end

for i = 1:length(sightLine)
    % Next, remove all sightline rows that include pixels outside the border
    removeRows = sightLine{i}(:,1) < 1 | sightLine{i}(:,2) < 1 | ...
        sightLine{i}(:,1) > size(matrix,1) | sightLine{i}(:,2) > size(matrix,2);
    sightLine{i}(removeRows,:) = [];
    % Then, write the categorical number of each line to 'v', and it's
    % location along the sightLine to 'I'
    for j = 1:size(sightLine{i},1)
        v(i,1:2) = matrix(sightLine{i}(j,1),sightLine{i}(j,2),1:2);
        v(i,3) = any(matrix(sightLine{i}(j,1),sightLine{i}(j,2),3:end));
        I(i) = j;
        if any(v(i,:) ~= 0)
            sightLine{i}(j+1:end,:) = [];
            break
        end
    end
end

I = I/(viewDistance*stepsize);
v(v>0) = 1;
sight = [I(:);v(:)];