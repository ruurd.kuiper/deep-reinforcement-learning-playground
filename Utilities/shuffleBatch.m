function [exp_batch, G_batch, r_old_batch, D_batch]  = shuffleBatch(miniBatchSize, experience, G, D)
maxSteps = numel(G);
I = randperm(maxSteps,miniBatchSize);
for t = 1:miniBatchSize
    exp_batch(:,:,:,t) = experience{I(t),1};
    G_batch(t) = G(I(t));
    r_old_batch(:,t) = experience{I(t),5};
    D_batch(:,t) = D(I(t)) .* experience{I(t),2};
end