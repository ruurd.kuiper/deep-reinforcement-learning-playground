function [l, gradients] = calculateGradientsActor(dlnet,dlX,D,eps,beta)
r = forward(dlnet,dlX);
r = max(min(r,1+eps),1-eps);
entropyBonus = beta*(mean(-sum(extractdata(r).*log(extractdata(r))))/log(size(r,1)));
l = crossentropy(r,D)+entropyBonus;

gradients = dlgradient(l,dlnet.Learnables);
end