function [l, gradients] = calculateGradientsActorNormal(dlnet,dlX,D)
%% For normal actor critic
l = crossentropy(softmax(forward(dlnet,dlX)),D);

gradients = dlgradient(l,dlnet.Learnables);
end