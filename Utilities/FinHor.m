function [G] = FinHor(reward,predCrit,gamma,lambda)
maxSteps = numel(reward);
for t = 1:maxSteps
    G(t) = 0;
    if t == maxSteps
        b = 0;
    else
        b = 1;
    end
    for k = t:maxSteps-1
        G(t) = G(t) + (gamma*lambda)^(k-t) * reward(k);
    end
    G(t) = G(t) + b * gamma ^ (maxSteps-t+1) * predCrit(maxSteps);
end