function [l, gradients] = calculateGradientsActorPPO(dlnet,dlX,D,r_old,eps,beta)
%% For PPO
r_new = forward(dlnet,dlX);
r_new = softmax(r_new);
r_new = r_new+1e-10;
r = r_new./r_old;
p1 = r;
p2 = max(min(r,1+eps),1-eps);
entropyBonus = beta*mean(mean(-(r_new .* log(r_new + 1e-10))));
l = min(crossentropy(p1,D),crossentropy(p2,D))+entropyBonus;

gradients = dlgradient(l,dlnet.Learnables);
end