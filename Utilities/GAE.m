function [D, G] = GAE(reward,predCritic,lambda,gamma)
maxSteps = numel(reward);
for t = 1:maxSteps
    D(t) = 0;
    if t == maxSteps
        b = 0;
    else
        b = 1;
    end
    for k = t:maxSteps-1
        delta(k) = reward(t) + b * gamma * predCritic(t+1) - predCritic(t);
        D(t) = D(t) + (gamma*lambda)^(k-t)*delta(k);
    end
    G(t) = D(t) - predCritic(t);
end
D = (D-mean(D))/std(D);
