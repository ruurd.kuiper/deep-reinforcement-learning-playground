function [sight,sightLine] = lineOfSight(matrix,location,rays,viewDistance)
% Create the lines
i = 1;
% for x = -viewDistance:viewDistance*2/rays:viewDistance
%     for y = -viewDistance:viewDistance*2/rays:viewDistance
%         if ~(x==0 && y==0)
%             for l = 1:viewDistance
%                 sightLine{i}(l,1) = round(l/viewDistance*x) + location(1);
%                 sightLine{i}(l,2) = round(l/viewDistance*y) + location(2);
%             end
%             i = i+1;
%         end
%     end
% end
% [location(1),location(2)] = deal(location(2),location(1));
%% First find the coordinates of the pixels in each straight sight line
for x = -viewDistance:viewDistance*2:viewDistance
    for y = -viewDistance:viewDistance*2/rays:viewDistance
        if ~(x==0 && y==0)
            for l = 1:viewDistance
                sightLine{i}(l,1) = round(l/viewDistance*x) + location(1);
                sightLine{i}(l,2) = round(l/viewDistance*y) + location(2);
            end
            i = i+1;
        end
    end
end

%% First find the coordinates of the pixels in each sight line
for x = -viewDistance+viewDistance*2/rays:viewDistance*2/rays:viewDistance-viewDistance*2/rays
    for y = -viewDistance:viewDistance*2:viewDistance
        if ~(x==0 && y==0)
            for l = 1:viewDistance
                sightLine{i}(l,1) = round(l/viewDistance*x) + location(1);
                sightLine{i}(l,2) = round(l/viewDistance*y) + location(2);
            end
            i = i+1;
        end
    end
end

% [~,idx] = unique(cell2mat(sightLine)', 'rows');
% sightLine = sightLine{idx};

[V, matrixCategorical] = max(matrix,[],3);
matrixCategorical(V == 0) = 0;
for i = 1:length(sightLine)
    % Next, remove all sightline rows that include pixels outside the border
    removeRows = sightLine{i}(:,1) < 1 | sightLine{i}(:,2) < 1 | ...
        sightLine{i}(:,1) > size(matrix,1) | sightLine{i}(:,2) > size(matrix,2);
    sightLine{i}(removeRows,:) = [];
    % Then, write the categorical number of each line to 'v', and it's
    % location along the sightLine to 'I'
    for j = 1:size(sightLine{i},1)
%         v(i) = matrixCategorical(sightLine{i}(j,1),sightLine{i}(j,2));
        v(i,1:2) = matrix(sightLine{i}(j,1),sightLine{i}(j,2),1:2);
        v(i,3) = any(matrix(sightLine{i}(j,1),sightLine{i}(j,2),3:end));
        I(i) = j;
        if any(v(i,:) ~= 0)
%             v(v>
            sightLine{i}(j+1:end,:) = [];
            break
        end
    end
end
% I(I==[]) = 0;
% v(v==[]) = 0;
I = I/viewDistance;
v(v>0) = 1;
sight = [I(:);v(:)];