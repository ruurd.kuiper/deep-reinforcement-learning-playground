function [l, gradients] = calculateGradientsCritic(dlnet,dlX,G)
y = forward(dlnet,dlX);
l = mse(y,G);
gradients = dlgradient(l,dlnet.Learnables);
