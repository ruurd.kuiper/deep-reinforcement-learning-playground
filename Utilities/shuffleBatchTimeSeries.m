function [exp_batch, G_batch, r_old_batch, D_batch]  = shuffleBatchTimeSeries(miniBatchSize, experience, G, D, minSteps)
maxSteps = numel(G);
I = randperm(maxSteps-minSteps,miniBatchSize)+minSteps;
exp_batch = experience{I(1),7}(:,:,:,:,end-minSteps+1:end);
for t = 1:miniBatchSize
    exp_batch = cat(4,exp_batch,experience{I(t),7}(:,:,:,:,end-minSteps+1:end));
    G_batch(t) = G(I(t));
    r_old_batch(:,t) = experience{I(t),5};
    D_batch(:,t) = D(I(t)) .* experience{I(t),2};
end
exp_batch = exp_batch(:,:,:,2:end,:);